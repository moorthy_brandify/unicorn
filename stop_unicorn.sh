#!/bin/bash

echo -en "Stopping gunicon ... ";
ps xa | grep gunicorn | grep 8000 | xargs kill -9 2> /dev/null
#killall -9 gunicorn 2> /dev/null
sleep 2
echo "Done!!"

