unicorn.page.pullreport = {

	unicornPage : null,
	
	initialize : function () {
		this.unicornPage = unicorn.page.initialize();
		this._setupListeners();
	},

	_setupListeners : function() {
		$( document ).ready(function() {
			$('#loadingdiv').show();
		      	setTimeout(unicorn.page.pullreport._getreport, 1500);
		});
	},

	_getreport : function() {
		$('#loadingdiv').show();
		$.ajax({
			context: this,
			type: "GET",
			url: "show",
			timeout: 5000,
			success: function(response) {
				$('#pullcontent').html(response);
				$('#loadingdiv').hide();
		      		setTimeout(unicorn.page.pullreport._getreport, 30000);
			},
			failure: function(errMsg) {
				//$('#loadingdiv').hide();
				//alert ("Error fetching data !!");
		      		setTimeout(unicorn.page.pullreport._getreport, 30000);
			},
			error: function(errMsg) {
				//$('#loadingdiv').hide();
				//alert ("Error fetching data !!");
		      		setTimeout(unicorn.page.pullreport._getreport, 30000);
			}
		});
	},

};
