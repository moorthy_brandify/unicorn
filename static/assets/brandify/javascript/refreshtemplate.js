unicorn.page.refreshtemplate = {

	unicornPage : null,
	urlToClear : null,

	initialize : function () {
		this.unicornPage = unicorn.page.initialize();
		this._setupListeners();
	},

	_setupListeners : function() {
		$('#dosubmit').click(this._postForm.bind(this));
	},

	_postForm: function() {
		$('#loadingdiv').show();
		this.urlToClear = $("#url").val();
		var account = $("#account").val();
		$.ajax({
			context: this,
			type: "POST",
			url: "/util/app/refreshtemplate/json/clear",
			timeout: 25000,
			data: JSON.stringify({ url : this.urlToClear, account : account }),
			contentType: "application/json; charset=utf-8",
			dataType: "json",
			success: function(response) {
				$('#responsecontent').html('<pre>' + JSON.stringify(response, null, 5) + '</pre>');
				if (response.status == 'OK') {
					$('#loadingdiv').hide();
				} else {
					$('#loadingdiv').hide();
					alert("Template refresh operation failed!!");
				}
			},
			failure: function(errMsg) {
				$('#loadingdiv').hide();
				alert ("Error fetching data !!");
			},
			error: function(errMsg) {
				$('#loadingdiv').hide();
				alert ("Error fetching data !!");
			}
		});
	}

};
