unicorn.page.dinetimestats = {

	unicornPage : null,
	responseData : null,
	dataTable : null,
	dataTableObject : null,
	tableDiv :  null,
	tableId :  null,

	tableCols : {
		'#dinetimestats' : [
				{ "title" : "Date", "data": "requestdate" },
				{ "title" : "ClientKey", "data": "clientkey" },
				{ "title" : "New Booking", "data": "addbooking", "className": "num_rightalign" },
				{ "title" : "Modify Booking", "data": "modifybooking", "className": "num_rightalign" },
				{ "title" : "Cancel Booking", "data": "cancelbooking", "className": "num_rightalign" },
				{ "title" : "API Hits", "data": "totalapi", "className": "num_rightalign" },
				{ "title" : "Total Guests Booked", "data": "bookingcount", "className": "num_rightalign" }
			],
		'#alldinetimestats' : [
				{ "title" : "Date", "data": "requestdate", "className": "num_rightalign" },
				{ "title" : "New Booking", "data": "addbooking", "className": "num_rightalign" },
				{ "title" : "Modify Booking", "data": "modifybooking", "className": "num_rightalign" },
				{ "title" : "Cancel Booking", "data": "cancelbooking", "className": "num_rightalign" },
				{ "title" : "API Hits", "data": "totalapi", "className": "num_rightalign" },
				{ "title" : "Total Guests Booked", "data": "bookingcount", "className": "num_rightalign" }
			],
	},

	tableaoCols : {
		'#dinetimestats' : [
				{ },
				{ },
				{ },
				{ },
				{ },
				{ },
				{ sClass : "numFormat" }
			],
		'#alldinetimestats' : [
				{ },
				{ },
				{ },
				{ },
				{ },
				{ }
			],
	},

	initialize : function () {
		this.unicornPage = unicorn.page.initialize();
		this._setupListeners();
	},

	_setupListeners : function() {
		$('#dosubmit').click(this._postForm.bind(this));
	},
	_validateForm: function() {
		
		isempty = 0;
		if ($("#clientkey").val() == '') {
			$("#clientkeydiv").addClass("has-error")
			isempty = 1;
		} else {
			$("#clientkeydiv").removeClass("has-error")
		}

		if ($("#sdate").val() == '') {
			$("#sdatediv").addClass("has-error")
			isempty = 1;
		} else {
			$("#sdatediv").removeClass("has-error")
		}
		if ($("#edate").val() == '') {
			$("#edatediv").addClass("has-error")
			isempty = 1;
		} else {
			$("#edatediv").removeClass("has-error")
		}

		if (isempty) {
			alert("Some inputs are still empty. Please input values");
			return false;
		}

		return true;
	},
	_postForm: function() {
		if (this._validateForm() == false) {
			return false;
		}
		var clientkey = $("#clientkey").val();
		var environment = $("#environment").val();
		var sdate  = $("#sdate").val();
		var edate  = $("#edate").val();
		var bylocation = $("#bylocation")[0].checked ? '1' : '0';

		var responseData = null;
		$('#dinetimestatsdiv').hide();
		$('#alldinetimestatsdiv').hide();
		$('#loadingdiv').show();
		if (bylocation == '1') {
			this.tableDiv = '#dinetimestatsdiv';
			this.tableId  = '#dinetimestats';
		} else {
			this.tableDiv = '#alldinetimestatsdiv';
			this.tableId  = '#alldinetimestats';
		}
		try {
			this.dataTable.destroy()
		} catch (e) {
		}

		$.ajax({
			context: this,
			type: "POST",
			url: "/util/app/dinetimestats/json/search",
			data: JSON.stringify({ clientkey : clientkey , environment : environment, sdate : sdate, edate : edate, bylocation : bylocation }),
			contentType: "application/json; charset=utf-8",
			dataType: "json",
			success: function(response) {
				if (response.status == 'OK') {
					this.responseData = response.data;
					this._initialiseTable();
					$(this.tableDiv).show();
					$('#loadingdiv').hide();
					this._loadTable();
				}
			},
			error: function(errMsg) {
				$('#loadingdiv').hide();
				alert ("Error fetching data !!");
			},
			failure: function(errMsg) {
				$('#loadingdiv').hide();
				alert ("Error fetching data !!");
			}
		});
	},
	_loadTable: function() {
		this.dataTableObject.fnClearTable();
		if (this.responseData.length) {
			this.dataTableObject.fnAddData(this.responseData);
		}
	},
	_initialiseTable: function() {
		tableId =  this.tableId;
		this.dataTable = $(tableId).DataTable( {
			dom: 'Blfrtip',
			buttons: [
					'copy', 'csv', 'excel', 'pdf', 'print'
			],
			"columns": this.tableCols[tableId],
			"language": {
				"emptyTable": "No matching records found !!",
			},
			"lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]]

		} );
		this.dataTableObject = $(tableId).dataTable();
	}

};
