unicorn.page.clearcache = {

	unicornPage : null,
	responseData : null,
	urlToClear : null,

	initialize : function () {
		this.unicornPage = unicorn.page.initialize();
		this._setupListeners();
	},

	_setupListeners : function() {
		$('#dosubmit').click(this._postForm.bind(this));
		$('#openbutton').click(this._popUpWindow.bind(this));
	},

	_postForm: function() {
		$('#loadingdiv').show();
		this.urlToClear = $("#url").val();
		var account = $("#account").val();
		var responseData = null;
		$.ajax({
			context: this,
			type: "POST",
			url: "/util/app/clearcache/json/clear",
			timeout: 25000,
			data: JSON.stringify({ url : this.urlToClear, account : account }),
			contentType: "application/json; charset=utf-8",
			dataType: "json",
			success: function(response) {
				if (response.status == 'OK') {
					this.responseData = response.data;
					$('#loadingdiv').hide();
					$('#promptSuccessModal').modal('show');
				} else {
					alert("Cache clear operation failed!!");
				}
			},
			failure: function(errMsg) {
				$('#loadingdiv').hide();
				alert ("Error fetching data !!");
			},
			error: function(errMsg) {
				$('#loadingdiv').hide();
				alert ("Error fetching data !!");
			}
		});
	},

	_popUpWindow: function() {
		var winProperties = "location=yes,height=570,width=520,scrollbars=yes,status=yes";
		var win = window.open(this.urlToClear, "_blank", winProperties);
		$('#promptSuccessModal').modal('hide');
	},
};
