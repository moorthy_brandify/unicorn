unicorn.page.byfeaturereport = {

	unicornPage : null,
	responseData : null,
	responseTitle : null,
	dataTable : null,
	dataTableObject : null,

	initialize : function () {
		this.unicornPage = unicorn.page.initialize();
		//this._initialiseTable();
		this._setupListeners();
	},

	_setupListeners : function() {
		$('#dosubmit').click(this._postForm.bind(this));
	},
	_postForm: function() {
		var productuid = $("#productuid").val();
		var responseData = null;
		$('#reportinfodiv').hide();
		$('.actform').hide();
		$('#loadingdiv').show();
		$.ajax({
			context: this,
			type: "POST",
			url: "/util/app/financereport/byfeature/json",
			data: JSON.stringify({ productuid : productuid }),
			contentType: "application/json; charset=utf-8",
			dataType: "json",
			success: function(response) {
				if (response.status == 'OK') {
					this.responseData = response.data;
					this.responseTitle = response.title;
					$('#reportinfodiv').show();
					$('#loadingdiv').hide();
					this._initialiseTable();
					this._loadTable();
				} else {
					$('#loadingdiv').hide();
					alert ("Error fetching data. May be due to invalid parameters !!");
				}
			},
                        error: function(errMsg) {
                                $('#loadingdiv').hide();
                                alert ("Error fetching data !!");
                        },
			failure: function(errMsg) {
                                $('#loadingdiv').hide();
                                alert ("Error fetching data !!");
			}
		});
	},
	_loadTable: function() {
		this.dataTableObject.fnClearTable();
		if (this.responseData.length) {
			this.dataTableObject.fnAddData(this.responseData);
		}
		this.dataTable.search('').draw()
		$('#reportinfodiv').show();
	},
	_initialiseTable: function() {
		cols = [];
		colsHtml = '';
		$(this.responseTitle).each(function (index) {
			cols[index] = { "data" : unicorn.page.byfeaturereport.responseTitle[index]};
			colsHtml += '<th>' + cols[index]['data'] + '</th>';
		});
		$('#reportheader').html(colsHtml);
		try {
			this.dataTable.destroy();
		} catch(e) {}
		this.dataTable = $('#reportinfo').DataTable( {
			dom: 'Bfrtip',
			buttons: [
					'copy', 'csv', 'excel', 'pdf', 'print'
			],
			columnDefs: [ {
			    orderable: false,
			    targets:   0
			} ],
			"columns": cols,
			"language": {
				"emptyTable": "No matching records found !!",
			},
		} );
		$('#reportinfo tbody')
			.on( 'mouseenter', 'td', function () {
				pageObject = unicorn.page.byfeaturereport;
				var colIdx = pageObject.dataTable.cell(this).index().column;
				var rowIdx = pageObject.dataTable.cell(this).index().row;

				$( pageObject.dataTable.rows().nodes() ).removeClass( 'highlight' );
				$( pageObject.dataTable.row( rowIdx ).nodes() ).addClass( 'highlight' );
			} );


		this.dataTableObject = $('#reportinfo').dataTable();
	},

};
