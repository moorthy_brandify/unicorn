unicorn.page.sdcreport= {

	unicornPage : null,
	responseData : null,
	dataTable : null,
	dataTableObject : null,

	initialize : function () {
		this.unicornPage = unicorn.page.initialize();
		this._initialiseTable();
		this._setupListeners();
	},

	_setupListeners : function() {
		$('#dosubmit').click(this._postForm.bind(this));
	},
	_postForm: function() {
		var schemaname  = $("#schemaname").val();
		var graphsource = $("#graphsource").val();
		var isclaimed   = $("#isclaimed").val();
		var responseData = null;
		$('#reportinfodiv').hide();
		$('.actform').hide();
		$('#loadingdiv').show();
		$.ajax({
			context: this,
			type: "POST",
			url: "/util/app/sdcreport/json/search",
			data: JSON.stringify({ schemaname : schemaname, graphsource: graphsource, isclaimed : isclaimed }),
			contentType: "application/json; charset=utf-8",
			dataType: "json",
			success: function(response) {
				if (response.status == 'OK') {
					this.responseData = response.data;
					$('#reportinfodiv').show();
					$('#loadingdiv').hide();
					this._loadTable();
				} else {
					$('#loadingdiv').hide();
					alert ("Error fetching data. May be due to invalid parameters !!");
				}
			},
                        error: function(errMsg) {
                                $('#loadingdiv').hide();
                                alert ("Error fetching data !!");
                        },
			failure: function(errMsg) {
                                $('#loadingdiv').hide();
                                alert ("Error fetching data !!");
			}
		});
	},
	_loadTable: function() {
		this.dataTableObject.fnClearTable();
		if (this.responseData.length) {
			this.dataTableObject.fnAddData(this.responseData);
		}
		this.dataTable.search('').draw()
		$('#reportinfodiv').show();
	},
	_initialiseTable: function() {
		this.dataTable = $('#reportinfo').DataTable( {
			dom: 'Bfrtip',
			buttons: [
					'copy', 'csv', 'excel', 'pdf', 'print'
			],
			columnDefs: [ {
			    orderable: false,
			    targets:   0
			} ],
			"columns": [
				{ "data":"schema" },
				{ "data":"graphsource" },
				{ "data":"uid" },
				{ "data":"clientkey" },
				{ "data":"graphid" },
				{ "data":"isclaimed" },
				{ "data":"isprimary" },
				{ "data":"effectivedate" },
				{ "data":"extragraphid1" },
				{ "data":"extragraphid2" },
				{ "data":"name" },
				{ "data":"address1" },
				{ "data":"city" },
				{ "data":"state" },
				{ "data":"postalcode" },
				{ "data":"country" },
				{ "data":"url" },
			],
			"language": {
				"emptyTable": "No matching records found !!",
			},
		} );
		$('#reportinfo tbody')
			.on( 'mouseenter', 'td', function () {
				pageObject = unicorn.page.sdcreport;
				var colIdx = pageObject.dataTable.cell(this).index().column;
				var rowIdx = pageObject.dataTable.cell(this).index().row;

				$( pageObject.dataTable.rows().nodes() ).removeClass( 'highlight' );
				$( pageObject.dataTable.row( rowIdx ).nodes() ).addClass( 'highlight' );
			} );

		this.dataTable.on( 'select', function ( e, dt, type, indexes ) {
			if ( type === 'row' ) {
				pageObject = unicorn.page.sdcreport;
				pageObject._refreshActionForm();
    			}
		});

		this.dataTable.on( 'deselect', function ( e, dt, type, indexes ) {
			if ( type === 'row' ) {
				pageObject = unicorn.page.sdcreport;
				pageObject._refreshActionForm();
    			}
		});

		this.dataTableObject = $('#reportinfo').dataTable();
	},

};
