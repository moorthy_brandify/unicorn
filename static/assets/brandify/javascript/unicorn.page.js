unicorn.page = {

	_strongRegex : null,
	_mediumRegex : null,
	_enoughRegex : null,

	initialize: function() {
		this._initialiseRegex();

		return this;
	},

	_initialiseRegex : function() {
		this._strongRegex = new RegExp("^(?=.{8,})(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9])(?=.*[^\w]).*$", "g");
		this._mediumRegex = new RegExp("^(?=.{7,})(((?=.*[A-Z])(?=.*[a-z]))|((?=.*[A-Z])(?=.*[0-9]))|((?=.*[a-z])(?=.*[0-9]))).*$", "g");
		this._enoughRegex = new RegExp("(?=.{6,}).*", "g");
	},
};
