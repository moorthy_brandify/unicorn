unicorn.page.profile = {

	_strongRegex : null,
	_mediumRegex : null,
	_enoughRegex : null,

	initialize : function () {
		this._setupListeners();
	},

	_initialiseRegex : function() {
		this._strongRegex = new RegExp("^(?=.{8,})(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9])(?=.*[^\w]).*$", "g");
		this._mediumRegex = new RegExp("^(?=.{7,})(((?=.*[A-Z])(?=.*[a-z]))|((?=.*[A-Z])(?=.*[0-9]))|((?=.*[a-z])(?=.*[0-9]))).*$", "g");
		this._enoughRegex = new RegExp("(?=.{6,}).*", "g");
	},

	_setupListeners : function() {
		$('#edit_user_details').click(this._enable_edit_user_details.bind(this));
		$('#cancelbutton_userdetails').click(this._showCancelModal.bind(this));
		$('#cancelbutton_pswdchange').click(this._showCancelModal.bind(this));
		$('#newpassword').keyup(this._passwordValidate.bind(this, '#newpassword'));
		$('#newpasswordconfirm').keyup(this._passwordValidate.bind(this, '#newpasswordconfirm'));
		$('#pswdform').submit(this._validatePasswordForm.bind(this));

	},
	_showCancelModal: function() {
		$('#cancelModal').modal('show');
		return false;
	},
	_validatePasswordForm: function() {

		$("#currentpassword_div").removeClass("has-error");
		$("#newpassword_div").removeClass("has-error");
		$("#newpasswordconfirm_div").removeClass("has-error");

		try {
			var currentpassword = $("#currentpassword").val();
			var newpassword = $("#newpassword").val();
			var newpasswordconfirm = $("#newpasswordconfirm").val();
			var isempty = 0;

			if (currentpassword.length == 0) {
				$("#currentpassword_div").addClass("has-error");
				isempty = 1;
			}
			if (newpassword.length == 0) {
				$("#newpassword_div").addClass("has-error");
				isempty = 1;
			}
			if (newpasswordconfirm.length == 0) {
				$("#newpasswordconfirm_div").addClass("has-error");
				isempty = 1;
			}

			$('#alertModalLabel').html('<b>Input Errors</b>');
			if (isempty == 1) {
				$('#alertModalMsg').html('One or more fields are empty.');
				$('#alertModal').modal('show');
				return false;
			}

			if (currentpassword == newpassword) {
				$("#currentpassword_div").addClass("has-error");
				$("#newpassword_div").addClass("has-error");
				$("#newpasswordconfirm_div").addClass("has-error");
				$('#alertModalMsg').html('Your current and new password cannot be same.');
				$('#alertModal').modal('show');
				return false;
			}

			if (newpassword != newpasswordconfirm) {
				$("#newpassword_div").addClass("has-error");
				$("#newpasswordconfirm_div").addClass("has-error");
				$('#alertModalMsg').html('Your new and confirmation passwords doesnot match.');
				$('#alertModal').modal('show');
				return false;
			}

			this._initialiseRegex();
			if (!this._mediumRegex.test(newpassword)) {
				$("#newpassword_div").addClass("has-error");
				$("#newpasswordconfirm_div").addClass("has-error");
				$('#alertModalMsg').html('Your new passwords does not pass basic security rules.');
				$('#alertModal').modal('show');
				return false;
			}
		} catch (exception) {
			$('#alertModalLabel').html('<b>Input Errors</b>');
			$('#alertModalMsg').html('Error occurred while validating your inputs.');
			$('#alertModal').modal('show');
			return false;
		}

		return true;
	},
	_passwordValidate: function(id) {

		this._initialiseRegex();
		var pwd = $(id).val();
		if (pwd.length==0) {
			$(id + '_strength').removeClass('btn-warning').removeClass('btn-success').addClass('btn-danger');
			$(id + '_strength_icon').removeClass('fa-check').removeClass('fa-minus').addClass('fa-times');
		} else if (false == this._enoughRegex.test(pwd)) {
			$(id + '_strength').removeClass('btn-warning').removeClass('btn-success').addClass('btn-danger');
			$(id + '_strength_icon').removeClass('fa-check').removeClass('fa-minus').addClass('fa-times');
		} else if (this._strongRegex.test(pwd)) {
			$(id + '_strength').removeClass('btn-danger').removeClass('btn-warning').addClass('btn-success');
			$(id + '_strength_icon').removeClass('fa-times').removeClass('fa-minus').addClass('fa-check');
		} else if (this._mediumRegex.test(pwd)) {
			$(id + '_strength').removeClass('btn-danger').removeClass('btn-success').addClass('btn-warning');
			$(id + '_strength_icon').removeClass('fa-check').removeClass('fa-times').addClass('fa-minus');
		} else {
			$(id + '_strength').removeClass('btn-warning').removeClass('btn-success').addClass('btn-danger');
			$(id + '_strength_icon').removeClass('fa-check').removeClass('fa-minus').addClass('fa-times');
		}
		$(id + '_strength').show();
	},
	_enable_edit_user_details: function() {
		$('#form_user_details').removeAttr('disabled');
		$('#edit_user_details').hide();
	},
	_disable_edit_user_details: function() {
		$('#form_user_details').attr('disabled','1');
	},
}
