unicorn.page.yelpreport= {

	unicornPage : null,
	responseData : null,
	dataTable : null,
	dataTableObject : null,

	initialize : function () {
		this.unicornPage = unicorn.page.initialize();
		this._initialiseTable();
		this._setupListeners();
	},

	_setupListeners : function() {
		$('#dosubmit').click(this._postForm.bind(this));
		$('#doaction').click(this._confirmAction.bind(this));
		$('#triggerbutton').click(this._postTrigger.bind(this));
	},
	_confirmAction: function() {
		
		var selectCount = this.dataTable.rows( { selected: true }).data().length;
		if (selectCount == 0) {
			return;
		};

		var selectedAction  = $('#reportaction').val();
		var selectedActionTxt   = 'Unknown';
		if (selectedAction == 'CLAIM') {
			selectedActionTxt = 'Claim Again';
		} else if (selectedAction == 'REVOKE') {
			selectedActionTxt = 'Revoke Claim';
		} else if (selectedAction == 'CLAIMBFILL')  {
			selectedActionTxt = 'Refresh Claim Status';
		} else if (selectedAction == 'CREATEBFILL') { 
			selectedActionTxt = 'Refresh Create Status';
		}

		var confirmationText = `
<ul>
	<li>Number of Graph Ids selected : <strong>` + selectCount  + `</strong></li>
	<li>Action to be Triggerred : <strong>` + selectedActionTxt + `</strong></li>
</ul>
<br/>
<span>
	<p style="text-align: center; font-weight: bold; font-family: cursive; font-size: 16px;">Do you want to Continue ... ?</p>
</span>`;

		$('.modal-body').html(confirmationText);
		$('#promptModal').modal('show')
	},
	_postTrigger: function() {
		
		var schemaname = $("#schemaname").val();
		var selectedAction  = $('#reportaction').val();
		var selectedRows = this.dataTable.rows( { selected: true }).data();
		var graphuids = '';
		selectedRows.toArray().forEach(function (item, index) {
			graphuids += item.uid + ','
		});

		$('#loadingdiv').show();

		var responseData = null;
		$.ajax({
			context: this,
			type: "POST",
			url: "/util/app/yelpreport/json/trigger",
			data: JSON.stringify({ schemaname : schemaname, action : selectedAction, graphuids: graphuids}),
			contentType: "application/json; charset=utf-8",
			dataType: "json",
			success: function(response) {
				if (response.status == 'OK') {
					this.responseData = response.data;
					$('#loadingdiv').hide();
					$('#promptModal').modal('hide')
					$('#dosubmit').click();
				} else {
					$('#loadingdiv').hide();
					$('#promptModal').modal('hide')
					alert ("Error occurred in Execution ... !!");
				}
			},
                        error: function(errMsg) {
                                $('#loadingdiv').hide();
				$('#promptModal').modal('hide')
				alert ("Error occurred in Execution ... !!");
                        },
			failure: function(errMsg) {
                                $('#loadingdiv').hide();
				$('#promptModal').modal('hide')
				alert ("Error occurred in Execution ... !!");
			}
		});
		return;
	},
	_postForm: function() {
		var schemaname = $("#schemaname").val();
		var reporttype = $("#reporttype").val();
		var isclaimed  = $("#isclaimed").val();
		var responseData = null;
		$('#reportinfodiv').hide();
		$('.actform').hide();
		$('#loadingdiv').show();
		$.ajax({
			context: this,
			type: "POST",
			url: "/util/app/yelpreport/json/search",
			data: JSON.stringify({ schemaname : schemaname, reporttype : reporttype, isclaimed : isclaimed }),
			contentType: "application/json; charset=utf-8",
			dataType: "json",
			success: function(response) {
				if (response.status == 'OK') {
					this.responseData = response.data;
					$('#reportinfodiv').show();
					$('#loadingdiv').hide();
					this._loadTable();
				} else {
					$('#loadingdiv').hide();
					alert ("Error fetching data. May be due to invalid parameters !!");
				}
			},
                        error: function(errMsg) {
                                $('#loadingdiv').hide();
                                alert ("Error fetching data !!");
                        },
			failure: function(errMsg) {
                                $('#loadingdiv').hide();
                                alert ("Error fetching data !!");
			}
		});
	},
	_loadTable: function() {
		this.dataTableObject.fnClearTable();
		this.responseData.forEach(function (item, index) {
			item._sel = '';
			item.yelpjobid = '<a href="/util/app/yelpapi/show/jobstatus/' + item.yelpjobid + '" target="_blank">' + item.yelpjobid + '</a>';
			item.graphid   = '<a href="/util/app/yelpapi/show/businessinfo/' + item.graphid + '" target="_blank">' + item.graphid + '</a>';
			item.extragraphid1 = '<a href="/util/app/yelpapi/show/businessinfo/' + item.extragraphid1 + '" target="_blank">' + item.extragraphid1 + '</a>';
		});
		if (this.responseData.length) {
			this.dataTableObject.fnAddData(this.responseData);
		}
		this.dataTable.search('').draw()
		$('#reportinfodiv').show();
	},
	_refreshActionForm: function() {
		pageObject = unicorn.page.yelpreport;
		if (pageObject.dataTable.rows( { selected: true }).data().length > 0) {
			$('.actform').show();
		} else {
			$('.actform').hide();
		}
	},
	_initialiseTable: function() {
		this.dataTable = $('#reportinfo').DataTable( {
			dom: 'Bfrtip',
			buttons: [
					'copy', 'csv', 'excel', 'pdf', 'print'
			],
			columnDefs: [ {
			    orderable: false,
			    className: 'select-checkbox',
			    targets:   0
			} ],
			select: {
			    style:    'multi',
			    selector: 'td:first-child'
			},
			"columns": [
				{ "data":"_sel", "orderable": "false" },
				{ "data":"schema" },
				{ "data":"uid" },
				{ "data":"clientkey" },
				{ "data":"isclaimed" },
				{ "data":"yelpaction" },
				{ "data":"yelpjobid" },
				{ "data":"yelpjobstatus" },
				{ "data":"effectivedate" },
				{ "data":"graphid" },
				{ "data":"extragraphid1" },
				{ "data":"extragraphid2" },
				{ "data":"name" },
				{ "data":"address1" },
				{ "data":"city" },
				{ "data":"state" },
				{ "data":"postalcode" },
				{ "data":"country" },
			],
			"language": {
				"emptyTable": "No matching records found !!",
			},
		} );
		$('#reportinfo tbody')
			.on( 'mouseenter', 'td', function () {
				pageObject = unicorn.page.yelpreport;
				var colIdx = pageObject.dataTable.cell(this).index().column;
				var rowIdx = pageObject.dataTable.cell(this).index().row;

				$( pageObject.dataTable.rows().nodes() ).removeClass( 'highlight' );
				$( pageObject.dataTable.row( rowIdx ).nodes() ).addClass( 'highlight' );
			} );

		this.dataTable.on( 'select', function ( e, dt, type, indexes ) {
			if ( type === 'row' ) {
				pageObject = unicorn.page.yelpreport;
				pageObject._refreshActionForm();
    			}
		});

		this.dataTable.on( 'deselect', function ( e, dt, type, indexes ) {
			if ( type === 'row' ) {
				pageObject = unicorn.page.yelpreport;
				pageObject._refreshActionForm();
    			}
		});

		this.dataTableObject = $('#reportinfo').dataTable();
	},

};
