unicorn.page.dashlets = {

	unicornPage : null,
	dashlets    : [],
	refreshtime : 300,
	
	initialize : function (opts) {
		this.unicornPage = unicorn.page.initialize();
		if (opts.hasOwnProperty('dashlets')) {
			this.dashlets = opts.dashlets;
		}
		if (opts.hasOwnProperty('refreshtime')) {
			this.refreshtime = opts.refreshtime;
		}

		if (this.dashlets.length) {
			for (var i = 0; i < this.dashlets.length; i++) {
				this.run(this.dashlets[i]);
			}
		}

		this._setupListeners();
	},

	_setupListeners : function() {
		
		$('.dashlet_refresh').on('click', function (event) {
			dashletid = $(this).parents('.dashlet').attr('id');
			if (dashletid) {
				unicorn.page.dashlets._getreport(dashletid);
			}
		}); 
		$('.dashlet_input').on('change', function (event) {
			dashletid = $(this).parents('.dashlet').attr('id');
			if (dashletid) {
				unicorn.page.dashlets._getreport(dashletid);
			}
		});
		$('#resize-dashboard-button').on('click', function (event) {
			unicorn.page.dashlets._toggleDashboardSize();
		});
		$('.dashlet_suspend').on('click', function (event) {
			dashletid = $(this).parents('.dashlet').attr('id');
			var env = $('#' + dashletid).find('.dashlet_input').val()
			if (!env) {
				return;
			}
			var jobhistoryuid = $('#' + dashletid).find('input[type=radio][name='+ env + '_jobhistoryuid]:checked').val()
			if (!dashletid) {
				return;
			}
			unicorn.page.dashlets._suspendjob(dashletid, env, jobhistoryuid);
		}); 
		$('.dashlet_prioritise').on('click', function (event) {
			dashletid = $(this).parents('.dashlet').attr('id');
			var env = $('#' + dashletid).find('.dashlet_input').val()
			if (!env) {
				return;
			}
			var jobhistoryuid = $('#' + dashletid).find('input[type=radio][name='+ env + '_jobhistoryuid]:checked').val()
			if (!dashletid) {
				return;
			}
			unicorn.page.dashlets._prioritisejob(dashletid, env, jobhistoryuid, '1');
		}); 
		$('.dashlet_deprioritise').on('click', function (event) {
			dashletid = $(this).parents('.dashlet').attr('id');
			var env = $('#' + dashletid).find('.dashlet_input').val()
			if (!env) {
				return;
			}
			var jobhistoryuid = $('#' + dashletid).find('input[type=radio][name='+ env + '_jobhistoryuid]:checked').val()
			if (!dashletid) {
				return;
			}
			unicorn.page.dashlets._prioritisejob(dashletid, env, jobhistoryuid, '0');
		}); 

	},

	_refreshListeners : function(env) {
		$('input[type=radio][name=' + env + '_jobhistoryuid]').on('change', function () {
			dashletid = $(this).parents('.dashlet').attr('id');
			$('#' + dashletid).find('.dashlet_suspend').each(function() { 
				$(this).show(); 
			});
			$('#' + dashletid).find('.dashlet_prioritise').each(function() { 
				$(this).show(); 
			});
			$('#' + dashletid).find('.dashlet_deprioritise').each(function() { 
				$(this).show(); 
			});
		}); 
	},

	_toggleDashboardSize: function() {
			var b = $('#resize-dashboard');
			console.log(b.attr("class"));
			if (b.hasClass("glyphicon-resize-full")) {
				b.removeClass("glyphicon-resize-full");
				b.addClass("glyphicon-resize-small");
				$('#page-wrapper').css('margin', '0 0 0 0')
				$('#menu-sidebar').hide();
			} else {
				b.removeClass("glyphicon-resize-small");
				b.addClass("glyphicon-resize-full");
				$('#page-wrapper').css('margin', '0 0 0 250px')
				$('#menu-sidebar').show();
			}
	},

	run : function(dashletid) {
		dashletname = $('#' + dashletid ).attr('dashlet-name');
		refreshtime = parseInt($('#' + dashletid ).attr('dashlet-refreshtime'));
		if (!refreshtime) {
			refreshtime = this.refreshtime; //Default 5mins
		}

		interval = refreshtime * 1000;

		if (!dashletname) {
			console.log ("Warn: dashlet-name attr missing for [" + dashletid + "]");
			return;
		}

		this._getreport(dashletid);
		this._schedule(dashletid, interval);
	},

	_schedule : function(dashletid, interval) {
		setTimeout (function() {
			unicorn.page.dashlets._execute(dashletid, interval);
		}, interval);
	},

	_execute : function (dashletid, interval) {
		this._getreport(dashletid);
		this._schedule(dashletid, interval);
	},

	_getreport : function(dashletid) {
		dashletname = $('#' + dashletid ).attr('dashlet-name');
		dashletparams = '?';
		$('#' + dashletid).find('.dashlet_input').each(function() { 
			key = $(this).attr('dashlet-input');
			val = $(this).val(); 
			dashletparams += key + "=" + val + "&";
		});
		$('#' + dashletid).find('.dashlet_loading').show();
		dashletparams  = encodeURI(dashletparams);
		$.ajax({
			context: this,
			type: "GET",
			url: "/dashlets/" + dashletname + "/show/" + dashletparams,
			timeout: 8000,
			success: function(response) {
				$('#' + dashletid).find('.dashlet_content').html(response);
				$('#' + dashletid).find('.dashlet_loading').hide();

				var env = $('#' + dashletid).find('.dashlet_input').val()
				if (env) {
					var title = 'Production';
					if (env == 'stag') {
						title = 'Staging';
					} else if (env == 'devl') {
						title = 'Development';
					}
					$('#' + dashletid).find('.fuploadqueue_title').html('Upload Queue - ' + title);
					unicorn.page.dashlets._refreshListeners(env);
				}

				$('#' + dashletid).find('.dashlet_suspend').hide();
				$('#' + dashletid).find('.dashlet_prioritise').hide();
				$('#' + dashletid).find('.dashlet_deprioritise').hide();
			},
			error: function(errMsg) {
				console.log("Error: While fetching dashlet content for " + dashletname);
				console.log("Reason: " + errMsg);
				$('#' + dashletid).find('.dashlet_content').html('<p>Error Fetching Data</p>');
				$('#' + dashletid).find('.dashlet_loading').hide();
			},
			failure: function(errMsg) {
				console.log("Error: While fetching dashlet content for " + dashletname);
				console.log("Reason: " + errMsg);
				$('#' + dashletid).find('.dashlet_content').html('<p>Error Fetching Data</p>');
				$('#' + dashletid).find('.dashlet_loading').hide();
			}
		});
	},

	_suspendjob : function(dashletid, environment, jobhistoryuid) {
		dashletname = $('#' + dashletid ).attr('dashlet-name');
		dashletparams = '?environment=' + environment + '&jobhistoryuid=' + jobhistoryuid;
		dashletparams  = encodeURI(dashletparams);
		$('#' + dashletid).find('.dashlet_loading').show();

		$.ajax({
			context: this,
			type: "GET",
			url: "/dashlets/" + dashletname + "/suspend/" + dashletparams,
			timeout: 8000,
			success: function(response) {
				unicorn.page.dashlets._getreport(dashletid);
			},
			error: function(errMsg) {
				console.log("Error: While suspending job for " + dashletname);
				console.log("Reason: " + errMsg);
				$('#' + dashletid).find('.dashlet_loading').hide();
			},
			failure: function(errMsg) {
				console.log("Error: While suspending job for " + dashletname);
				console.log("Reason: " + errMsg);
				$('#' + dashletid).find('.dashlet_loading').hide();
			}
		});
	},

	_prioritisejob : function(dashletid, environment, jobhistoryuid, priority) {
		if (!priority) {
			priority = '0';
		}
		dashletname = $('#' + dashletid ).attr('dashlet-name');
		dashletparams = '?environment=' + environment + '&jobhistoryuid=' + jobhistoryuid + '&priority=' + priority;
		dashletparams  = encodeURI(dashletparams);
		$('#' + dashletid).find('.dashlet_loading').show();

		$.ajax({
			context: this,
			type: "GET",
			url: "/dashlets/" + dashletname + "/prioritise/" + dashletparams,
			timeout: 8000,
			success: function(response) {
				unicorn.page.dashlets._getreport(dashletid);
			},
			error: function(errMsg) {
				console.log("Error: While prioritising job for " + dashletname);
				console.log("Reason: " + errMsg);
				$('#' + dashletid).find('.dashlet_loading').hide();
			},
			failure: function(errMsg) {
				console.log("Error: While prioritising job for " + dashletname);
				console.log("Reason: " + errMsg);
				$('#' + dashletid).find('.dashlet_loading').hide();
			}
		});
	},

};
