unicorn.page.resetpassword = {

	unicornPage : null,

	initialize : function () {
		this.unicornPage = unicorn.page.initialize();
		this._setupListeners();
	},

	_setupListeners : function() {
		$('#newpassword').keyup(this._passwordValidate.bind(this, '#newpassword'));
		$('#newpasswordconfirm').keyup(this._passwordValidate.bind(this, '#newpasswordconfirm'));
		$('#resetpswdform').submit(this._validatePasswordForm.bind(this));

	},
	_validatePasswordForm: function() {

		$("#newpassword_div").removeClass("has-error");
		$("#newpasswordconfirm_div").removeClass("has-error");

		try {
			var newpassword = $("#newpassword").val();
			var newpasswordconfirm = $("#newpasswordconfirm").val();
			var isempty = 0;

			if (newpassword.length == 0) {
				$("#newpassword_div").addClass("has-error");
				isempty = 1;
			}
			if (newpasswordconfirm.length == 0) {
				$("#newpasswordconfirm_div").addClass("has-error");
				isempty = 1;
			}

			$('#alertModalLabel').html('<b>Input Errors</b>');
			if (isempty == 1) {
				$('#alertModalMsg').html('One or more fields are empty.');
				$('#alertModal').modal('show');
				return false;
			}

			if (newpassword != newpasswordconfirm) {
				$("#newpassword_div").addClass("has-error");
				$("#newpasswordconfirm_div").addClass("has-error");
				$('#alertModalMsg').html('Your new and confirmation passwords doesnot match.');
				$('#alertModal').modal('show');
				return false;
			}

			this.unicornPage = unicorn.page.initialize();
			if (!this.unicornPage._mediumRegex.test(newpassword)) {
				$("#newpassword_div").addClass("has-error");
				$("#newpasswordconfirm_div").addClass("has-error");
				$('#alertModalMsg').html('Your new passwords does not pass basic security rules.');
				$('#alertModal').modal('show');
				return false;
			}
		} catch (exception) {
			$('#alertModalLabel').html('<b>Input Errors</b>');
			$('#alertModalMsg').html('Error occurred while validating your inputs.');
			$('#alertModal').modal('show');
			return false;
		}

		return true;
	},
	_passwordValidate: function(id) {

		var pwd = $(id).val();
		this.unicornPage = unicorn.page.initialize();
		if (pwd.length==0) {
			$(id + '_strength').removeClass('btn-warning').removeClass('btn-success').addClass('btn-danger');
			$(id + '_strength_icon').removeClass('fa-check').removeClass('fa-minus').addClass('fa-times');
		} else if (false == this.unicornPage._enoughRegex.test(pwd)) {
			$(id + '_strength').removeClass('btn-warning').removeClass('btn-success').addClass('btn-danger');
			$(id + '_strength_icon').removeClass('fa-check').removeClass('fa-minus').addClass('fa-times');
		} else if (this.unicornPage._strongRegex.test(pwd)) {
			$(id + '_strength').removeClass('btn-danger').removeClass('btn-warning').addClass('btn-success');
			$(id + '_strength_icon').removeClass('fa-times').removeClass('fa-minus').addClass('fa-check');
		} else if (this.unicornPage._mediumRegex.test(pwd)) {
			$(id + '_strength').removeClass('btn-danger').removeClass('btn-success').addClass('btn-warning');
			$(id + '_strength_icon').removeClass('fa-check').removeClass('fa-times').addClass('fa-minus');
		} else {
			$(id + '_strength').removeClass('btn-warning').removeClass('btn-success').addClass('btn-danger');
			$(id + '_strength_icon').removeClass('fa-check').removeClass('fa-minus').addClass('fa-times');
		}
		$(id + '_strength').show();
	},
	_enable_edit_user_details: function() {
		$('#form_user_details').removeAttr('disabled');
		$('#edit_user_details').hide();
	},
	_disable_edit_user_details: function() {
		$('#form_user_details').attr('disabled','1');
	},
};
