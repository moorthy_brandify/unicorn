unicorn.page.yelpapi = {

	unicornPage : null,
	responseData : null,

	initialize : function () {
		this.unicornPage = unicorn.page.initialize();
		this._setupListeners();

		var checked = $('#yelpapijob').is(':checked');
		if (checked == true) {
			$('#yelpjobid').attr('placeholder', 'Yelp Job ID');
		}

		checked = $('#yelpapibiz').is(':checked');
		if (checked == true) {
			$('#yelpjobid').attr('placeholder', 'Yelp Business ID');
		}
	},

	_setupListeners : function() {
		$('#dosubmit').click(this._postForm.bind(this));
		$('#yelpapijob').on('change', function (event) {
			var checked = $('#yelpapijob').is(':checked');
			if (checked == true) {
				$('#yelpjobid').attr('placeholder', 'Yelp Job ID');
			}
		});
		$('#yelpapibiz').on('change', function (event) {
			var checked = $('#yelpapibiz').is(':checked');
			if (checked == true) {
				$('#yelpjobid').attr('placeholder', 'Yelp Business ID');
			}
		});
	},

	_postForm: function() {
		$('#loadingdiv').show();
	}
};
