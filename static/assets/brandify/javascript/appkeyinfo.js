unicorn.page.appkeyinfo = {

	unicornPage : null,
	responseData : null,
	dataTable : null,
	dataTableObject : null,

	initialize : function () {
		this.unicornPage = unicorn.page.initialize();
		this._initialiseTable();
		this._setupListeners();
	},

	_setupListeners : function() {
		$('#dosubmit').click(this._postForm.bind(this));
		$('#appkeyinfo tbody').on('click', 'td.details-control', function () {
			var tr = $(this).closest('tr');
			var row = unicorn.page.appkeyinfo.dataTable.row( tr );
			if ( row.child.isShown() ) {
				row.child.hide();
				tr.removeClass('shown');
			} else {
				row.child( unicorn.page.appkeyinfo._expandTable(row.data()) ).show();
				tr.addClass('shown');
			}
		} ).bind(this);


	},
	_postForm: function() {
		var searchinput = $("#searchinput").val();
		var environment = $("#environment").val();
		var responseData = null;
		$('#appkeyinfodiv').hide();
		$('#loadingdiv').show();
		$.ajax({
			context: this,
			type: "POST",
			url: "/util/app/appkeyinfo/json/search",
			data: JSON.stringify({ input : searchinput, environment : environment }),
			contentType: "application/json; charset=utf-8",
			dataType: "json",
			timeout: 7000,
			success: function(response) {
				if (response.status == 'OK') {
					this.responseData = response.data;
					$('#appkeyinfodiv').show();
					$('#loadingdiv').hide();
					this._loadTable();
				}
			},
			error: function(errMsg) {
				$('#loadingdiv').hide();
				alert ("Error fetching data !!");
			},
			failure: function(errMsg) {
				$('#loadingdiv').hide();
				alert ("Error fetching data !!");
			}
		});
	},
	_loadTable: function() {
		this.dataTableObject.fnClearTable();
		if (this.responseData.length) {
			this.dataTableObject.fnAddData(this.responseData);
		}
		$('#appkeyinfodiv').show();
	},
	_initialiseTable: function() {
		this.dataTable = $('#appkeyinfo').DataTable( {
			dom: 'Bfrtip',
			buttons: [
					'copy', 'csv', 'excel', 'pdf', 'print'
			],
			"columns": [
				{
					"className":      'details-control',
					"orderable":      false,
					"data":           {},
					"defaultContent": ''
				},
				{ "data": "schemaname" },
				{ "data": "name" },
				{ "data": "appkey" },
				{ "data": "authurl" },
				{ "data": "authip" }
			],
			"language": {
				"emptyTable": "No matching records found !!",
			},
			"order": [[1, 'asc']]
		} );
		this.dataTableObject = $('#appkeyinfo').dataTable();
	},
	_expandTable ( d ) {
                  // `d` is the original data object for the row

	Object.keys(d).forEach(function(key) {
		if (d[key] == '0' || d[key] == 'false') {
			d[key] = 'False';
			d[key] = '<i class="fa fa-times fa-fw pull-right"></i>';
		} else if  (d[key] == '1' || d[key] == 'true') {
			d[key] = 'True';
			d[key] = '<i class="fa fa-check fa-fw pull-right"></i>';
		} else if (d[key] == null) {
			d[key] = '';
		}
	});
	
		return `
<div class="row">
	<div class="col-lg-4">
		<div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-map-marker fa-fw"></i> Locator Configuration
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="list-group small">
                                <a href="#" class="list-group-item">
                                    <em> Locator Name </em>
                                    <span class="pull-right"> ` + d.name + `
                                    </span>
                                </a>
                                <a href="#" class="list-group-item">
                                    <em> Data View </em>
                                    <span class="pull-right"> ` + d.dataview + `
                                    </span>
                                </a>
                                <a href="#" class="list-group-item">
                                    <em> Expiration Date</em>
                                    <span class="pull-right"> ` + d.expdate+ `
                                    </span>
                                </a>
                                <a href="#" class="list-group-item">
                                    <em> Default Radius </em>
                                    <span class="pull-right"> ` + d.radius + `
                                    </span>
                                </a>
                                <a href="#" class="list-group-item">
                                    <em> Radius UOM </em>
                                    <span class="pull-right"> ` + d.radiusuom + `
                                    </span>
                                </a>
                                <a href="#" class="list-group-item">
                                    <em> Time View </em>
                                    <span class="pull-right"> ` + d.timeuom + `
                                    </span>
                                </a>
                                <a href="#" class="list-group-item">
                                    <em> Icon </em>
                                    <span class="pull-right"> ` + d.icon + `
                                    </span>
                                </a>
                                <a href="#" class="list-group-item">
                                    <em> Localsite URL Category </em>
                                    <span class="pull-right"> ` + d.urlstructure + `
                                    </span>
                                </a>
                                <a href="#" class="list-group-item">
                                    <em> Google Static Map </em>
                                    <span class="pull-right"> ` + d.gsmap + `
                                    </span>
                                </a>
                            </div>
                            <!-- /.list-group -->
                        </div>
                        <!-- /.panel-body -->
		</div>
	</div>
	<div class="col-lg-4">
		<div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-asterisk fa-fw"></i> Authorized Features
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="list-group small">
                                <a href="#" class="list-group-item">
                                    <em> Demo </em>
                                    <span class="pull-right"> ` + d.demo + `
                                    </span>
                                </a>
                                <a href="#" class="list-group-item">
                                    <em> Use Mass Merchant </em>
                                    <span class="pull-right"> ` + d.usemassmerchant + `
                                    </span>
                                </a>
                                <a href="#" class="list-group-item">
                                    <em> Use Territory </em>
                                    <span class="pull-right"> ` + d.useterritory + `
                                    </span>
                                </a>
                                <a href="#" class="list-group-item">
                                    <em> Use ClientSide Geocoding </em>
                                    <span class="pull-right"> ` + d.clientsidegeocoding + `
                                    </span>
                                </a>
                                <a href="#" class="list-group-item">
                                    <em> Use ClientSide Directions </em>
                                    <span class="pull-right"> ` + d.clientsidedirections+ `
                                    </span>
                                </a>
                                <a href="#" class="list-group-item">
                                    <em> Use Parent </em>
                                    <span class="pull-right"> ` + d.useparent + `
                                    </span>
                                </a>
                                <a href="#" class="list-group-item">
                                    <em> Mobile Locator </em>
                                    <span class="pull-right"> ` + d.mobile + `
                                    </span>
                                </a>
                                <a href="#" class="list-group-item">
                                    <em> Local Sites </em>
                                    <span class="pull-right"> ` + d.localsites + `
                                    </span>
                                </a>
                                <a href="#" class="list-group-item">
                                    <em> Create Sitemap </em>
                                    <span class="pull-right"> ` + d.createsitemap + `
                                    </span>
                                </a>
                            </div>
                            <!-- /.list-group -->
                        </div>
                        <!-- /.panel-body -->
		</div>
	</div>
	<div class="col-lg-4">
		<div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-truck fa-fw"></i> Mapping & Driving Directions
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="list-group small">
                                <a href="#" class="list-group-item">
                                    <em> Return Driving Directions </em>
                                    <span class="pull-right"> ` + d.returndrivingdirections + `
                                    </span>
                                </a>
                                <a href="#" class="list-group-item">
                                    <em> Language </em>
                                    <span class="pull-right"> ` + d.language+ `
                                    </span>
                                </a>
                                <a href="#" class="list-group-item">
                                    <em> Platform </em>
                                    <span class="pull-right"> ` + d.platform + `
                                    </span>
                                </a>
                                <a href="#" class="list-group-item">
                                    <em> Road Preference </em>
                                    <span class="pull-right"> ` + d.roadpreference + `
                                    </span>
                                </a>
                                <a href="#" class="list-group-item">
                                    <em> Return Route Geometry </em>
                                    <span class="pull-right"> ` + d.returnroutegeometry + `
                                    </span>
                                </a>
                                <a href="#" class="list-group-item">
                                    <em> Map Vendor </em>
                                    <span class="pull-right"> ` + d.mapvendor + `
                                    </span>
                                </a>
                                <a href="#" class="list-group-item">
                                    <em> Proximity </em>
                                    <span class="pull-right"> ` + d.proximity + `
                                    </span>
                                </a>
                                <a href="#" class="list-group-item">
                                    <em> Route Preference </em>
                                    <span class="pull-right"> ` + d.routepreference + `
                                    </span>
                                </a>
                                <a href="#" class="list-group-item">
                                    <em> GME </em>
                                    <span class="pull-right"> ` + d.gme + `
                                    </span>
                                </a>
                            </div>
                            <!-- /.list-group -->
                        </div>
                        <!-- /.panel-body -->
		</div>
	</div>
</div>

`;
              }

};
