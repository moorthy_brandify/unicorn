"""main URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.conf.urls import include
from django.contrib import admin
from django.views import static

from main import views as main_views
from main import dashletviews as dashlet_views
from core import views as core_views

import settings;

urlpatterns = [
    url(r'^admin/', admin.site.urls),

    url(r'^login/', core_views.login),
    url(r'^dologin', core_views.dologin),
    url(r'^dologout', core_views.dologout),

    url(r'^home/', main_views.home),
    url(r'^dashlets/(?P<appname>\w+)/(?P<action>\w+).*$', dashlet_views.rundashlet),
    url(r'^dashlets/(?P<appname>\w+)/(?P<action>\w+)/(?P<params>.*)$', dashlet_views.rundashlet),
    url(r'^authrequest/(?P<requestid>.*)$', main_views.authrequest),
    url(r'^core/', include('core.urls')),
    url(r'^api/', include('core.urls')),
    url(r'^util/', include('util.urls')),
    url(r'^main/epage/(?P<pagepath>.*)$', main_views.externalpage),
    url(r'^static/(?P<path>.*)$', static.serve, {'document_root': settings.STATIC_DIR, 'show_indexes':False}),

    #url(r'^$', include('core.urls')),
    url(r'^$', main_views.home),
    url(r'^.*$', main_views.home),
    #url(r'^$', main_views.index),
]
