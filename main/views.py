from django.http import HttpResponse
from django.template import loader
from django.shortcuts import render_to_response, redirect
from django.contrib.auth.decorators import login_required

from session import Context
import constants.staticpages

def index(request):
    return HttpResponse("Hello Viewer, You're at the main index.")


@login_required(login_url='/login/')
def home(request):

        if (request.user == 'AnonymousUser'):
                return redirect('/')

	templateData = Context(request, 'templatevars')
        template = loader.get_template('core/main.html')

        return HttpResponse(template.render(templateData.summarizeContext(), request))

@login_required(login_url='/login/')
def externalpage(request, pagepath):
	epage = constants.staticpages.STATICPAGES.get(pagepath)
	if epage:
        	template = loader.get_template('core/iframepage.html')
	else:
		return redirect('/home')

	templateData = Context(request, 'templatevars')
	templateData.setData('epage', epage)

        return HttpResponse(template.render(templateData.summarizeContext(), request))

def authrequest(request, requestid):

	import core.models
	import json

	authrequestRecord = None
	try:
		authrequestRecord = core.models.AuthRequests.objects.get(uuid=requestid)
	except:
		return redirect('/')


	requestData = json.loads(authrequestRecord.data)
	request.session['authrequest'] = requestData;
	request.session['authrequestid'] = requestid

	return redirect(requestData['redirectcontext'])
