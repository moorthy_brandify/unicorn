import smtplib
from email.mime.text import MIMEText

def sendemail (maildata):

	toaddr      = maildata['to']
	subject     = maildata['subject']
	message     = maildata['message']

	fromaddr    = "support@brandify.com"
	if maildata.has_key('from'):
		fromaddr    = maildata['from']

	contenttype = 'plain';
	if maildata.has_key('contenttype'):
		contenttype = maildata['contenttype']

	mimeversion = '1.0';
	if maildata.has_key('mimeversion'):
		mimeversion = maildata['mimeversion']

	ccaddr = None
	if maildata.has_key('cc'):
		ccaddr = maildata['cc']

	bccaddr = None
	if maildata.has_key('bcc'):
		ccaddr = maildata['bcc']

	
	mimemsg = MIMEText(message, _subtype=contenttype)
	mimemsg['From'] = fromaddr
	mimemsg['To']   = toaddr
	mimemsg['Subject']   = subject
	mimemsg['MIME-Version']   = mimeversion
	if  ccaddr != None:
		mimemsg['CC']   = ccaddr
	if  bccaddr != None:
		mimemsg['BCC']   = bccaddr

	emailobject = smtplib.SMTP('localhost')
	emailobject.sendmail(fromaddr, toaddr, mimemsg.as_string());
