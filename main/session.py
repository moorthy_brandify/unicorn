class Context:

	_requestObject = None
	_context = None

	def __init__ (self, obj, context='defaultcontext'):
		self._requestObject = obj
		self._context = context
		self._requestObject.session[self._context]  = {}

		self._setSessionInfo()

	def _setSessionInfo (self):

		sessionInfo = {}
		sessionInfo['user'] = '%s'%(self._requestObject.user)
		if (self._requestObject.user.is_authenticated()):
			sessionInfo['is_authenticated'] = 1
		else:
			sessionInfo['is_authenticated'] = 0

		sessionInfo['unicorn_baseurl'] = 'https://unicorn.dev.where2getit.com'
		sessionInfo['unicorn_loginurl'] = 'https://unicorn.dev.where2getit.com/login'

		if self._requestObject.session.has_key('userinfo'):
			sessionInfo['unicorn_userinfo'] = self._requestObject.session['userinfo']

		self.setData('session', sessionInfo)

	def setData (self, key, val):

		if (isinstance(val, (list, dict, int, str))):
			self._requestObject.session[self._context][key] = val
		else:
			self._requestObject.session[self._context][key] = val
			#for attr, attrval in val.__dict__.iteritems():
			#	self._requestObject.session[self._context][key][attr] = attrval

		return

	def getData (self, key):
		try:
			return self._requestObject.session[self._context][key]
		except:
			return None

	def delData (self, key):
		try:
			del self._requestObject.session[self._context][key]
		except:
			return

	def summarizeContext (self):
		retContext = {}
		try:
			for key, val in self._requestObject.session[self._context].iteritems():
				retContext[key] = val
			return retContext
		except Exception as e:
			pass
			return
