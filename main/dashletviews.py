from django.http import HttpResponse, JsonResponse

from django.template import loader
from django.shortcuts import render_to_response, redirect
from django.contrib.auth.decorators import login_required

from session import Context
import constants.environments

import re
import json
import urllib2

templateData = None
appName   = None
appAction = None
appParams = None

def index(request):
	return HttpResponse("Hello Viewer, This is a Dashlet.")


@login_required(login_url='/login/')
def rundashlet(request, appname, action=None, params=None):

	if (request.user == 'AnonymousUser'):
		return redirect('/')

	global templateData, appName, appAction, appParams

	templateData = Context(request, 'templatevars')
	appName   = appname
	appAction = action
	appParams = params

	if appname == 'fuploadqueue':
		return fuploadqueue(request)

	template = loader.get_template('core/main.html')

	return HttpResponse(template.render(templateData.summarizeContext(), request))

def fuploadqueue(request):
	global templateData, appName, appAction, appParams

	if (request.GET.has_key('environment')):
		environment = request.GET['environment']
	else:
		environment = 'prod'

	apiKey = constants.environments.MAPPINGS[environment]['apikey']
	apiUrl = constants.environments.MAPPINGS[environment]['dbqueryurl']

	template = loader.get_template('main/dashlets/' + appName + ".html")


	queueResponse = {}

	if (appAction == 'suspend'):
		try:
			if (request.GET.has_key('jobhistoryuid')):
				jobhistoryuid = request.GET['jobhistoryuid']
			else:
				raise Exception('Backend API Error')
			sqlQuery = "update w2gi.jobqueue set status ='suspended' where jobhistoryuid='%s'"%(jobhistoryuid);
			remoteJsonReq = {
				'apikey' : apiKey,
				'query'  : sqlQuery 
			}
			reqObj = urllib2.Request(apiUrl)
			reqObj.add_header('Content-Type', 'application/json')
			resObj = urllib2.urlopen(reqObj, json.dumps(remoteJsonReq))
			resCode = resObj.getcode()
			if (resCode == 200):
				respMsg  = resObj.read()
				respJson = json.loads(respMsg)
				if (respJson['status'] == 200):
					queueResponse['status'] = 'OK'
					queueResponse['data']   = respJson['data']
					queueResponse['count']  = respJson['count']
				else:
					raise Exception('Backend API Error')
			else:
				raise Exception('Backend API Error')

		except Exception as e:
			return JsonResponse({'status' : 'Not OK', 'message' : 'Error while performing suspend job'})

		return JsonResponse({'status' : 'OK', 'message' : 'Job Suspended Successfully'})
	elif (appAction == 'prioritise'):
		try:
			if (request.GET.has_key('jobhistoryuid')):
				jobhistoryuid = request.GET['jobhistoryuid']
			else:
				raise Exception('Backend API Error')

			if (request.GET.has_key('priority')):
				priority = request.GET['priority']
			else:
				priority = '0';

			sqlQuery = "update w2gi.jobqueue set priority ='%s' where jobhistoryuid='%s'"%(priority, jobhistoryuid);
			remoteJsonReq = {
				'apikey' : apiKey,
				'query'  : sqlQuery 
			}
			reqObj = urllib2.Request(apiUrl)
			reqObj.add_header('Content-Type', 'application/json')
			resObj = urllib2.urlopen(reqObj, json.dumps(remoteJsonReq))
			resCode = resObj.getcode()
			if (resCode == 200):
				respMsg  = resObj.read()
				respJson = json.loads(respMsg)
				if (respJson['status'] == 200):
					queueResponse['status'] = 'OK'
					queueResponse['data']   = respJson['data']
					queueResponse['count']  = respJson['count']
				else:
					raise Exception('Backend API Error')
			else:
				raise Exception('Backend API Error')

		except Exception as e:
			return JsonResponse({'status' : 'Not OK', 'message' : 'Error while performing prioritise job'})

		return JsonResponse({'status' : 'OK', 'message' : 'Job Suspended Successfully'})
	else:
		try:
			#sqlQuery = "select jobhistoryuid, jobtype, schema, fileactions, status, authuser, priority, to_char(entrydate, 'MM/DD/YYYY HH:MI:SS') as entrydate from w2gi.priority_jobqueue WHERE status IN ('waiting', 'running', 'ready', 'processing', 'on-hold', 'hold', 'stop', 'STOP') and jobtype='locations' order by priority desc, entrydate asc"
			sqlQuery = "select jobhistoryuid, jobtype, schema, fileactions, status, authuser, priority, to_char(entrydate, 'MM/DD/YYYY HH:MI:SS') as entrydate from w2gi.priority_jobqueue WHERE status IN ('waiting', 'running', 'ready', 'processing', 'on-hold', 'hold', 'stop', 'STOP') order by priority desc, entrydate asc"
			remoteJsonReq = {
				'apikey' : apiKey,
				'query'  : sqlQuery 
			}

			reqObj = urllib2.Request(apiUrl)
			reqObj.add_header('Content-Type', 'application/json')
			resObj = urllib2.urlopen(reqObj, json.dumps(remoteJsonReq))
			resCode = resObj.getcode()
			if (resCode == 200):
				respMsg  = resObj.read()
				respJson = json.loads(respMsg)
				if (respJson['status'] == 200):
					queueResponse['status'] = 'OK'
					queueResponse['data']   = respJson['data']
					queueResponse['count']  = respJson['count']
					queueResponse['environment']  = environment
				else:
					raise Exception('Backend API Error')
			else:
				raise Exception('Backend API Error')
		except Exception as e:
			return HttpResponse("Error while fetching the queue status !!")

		templateData.setData('queuedata', queueResponse)
		return HttpResponse(template.render(templateData.summarizeContext(), request))

