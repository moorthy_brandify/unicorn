STATICPAGES = {
'clearvarnishcache'		: {
					'title'		: 'Clear Varnish Cache',
					'pageurl' 	: '/util/app/clearcache/show/',
				},
'uploadqueue/development'	: {
					'title'		: 'Fileupload Queue - Development',
					'pageurl' 	: 'https://zeus.dev.where2getit.com/templates/upload-queue.tmpl?system=dev',
				},
'uploadqueue/staging'		: {
					'title'		: 'Fileupload Queue -  Staging',
					'pageurl'	: 'https://zeus.dev.where2getit.com/templates/upload-queue.tmpl?system=stag',
				},
'uploadqueue/production'	: {
					'title'		: 'Fileupload Queue - Production',
					'pageurl'	: 'https://zeus.dev.where2getit.com/templates/upload-queue.tmpl?system=prod',
				},
'locationaudit/development'	: {
					'title'		: 'Location Audit - Development',
					'pageurl'	: 'https://zeus.dev.where2getit.com/templates/query-audit.tmpl?system=dev',
				},
'locationaudit/staging'	: {
					'title'		: 'Location Audit - Staging',
					'pageurl'	: 'https://zeus.dev.where2getit.com/templates/query-audit.tmpl?system=stag',
				},
'locationaudit/production'	: {
					'title'		: 'Location Audit - Production',
					'pageurl'	: 'https://zeus.dev.where2getit.com/templates/query-audit.tmpl?system=prod',
				},
'querymappings/development'	: {
					'title'		: 'Query Mappings - Development',
					'pageurl'	: 'https://zeus.dev.where2getit.com/templates/query-locator.tmpl?system=dev',
				},
'querymappings/staging'	: {
					'title'		: 'Query Mappings - Staging',
					'pageurl'	: 'https://zeus.dev.where2getit.com/templates/query-locator.tmpl?system=stag',
				},
'querymappings/production'	: {
					'title'		: 'Query Mappings - Production',
					'pageurl'	: 'https://zeus.dev.where2getit.com/templates/query-locator.tmpl?system=prod',
				},
'locationmapping/development'	: {
					'title'		: 'Location/LDS Mapping- Development',
					'pageurl'	: 'https://zeus.dev.where2getit.com/templates/query-locator.tmpl?system=dev',
				},
'locationmapping/staging'	: {
					'title'		: 'Location/LDS Mapping- Staging',
					'pageurl'	: 'https://zeus.dev.where2getit.com/templates/query-locator.tmpl?system=stag',
				},
'locationmapping/production'	: {
					'title'		: 'Location/LDS Mapping- Production',
					'pageurl'	: 'https://zeus.dev.where2getit.com/templates/query-locator.tmpl?system=prod',
				},
'cbdinetime/staging'	: {
					'title'		: 'CrackerBarrel Wait status - Staging',
					'pageurl'	: 'https://hosted.where2stageit.com/cgi-bin/dinetimewait.cgi',
				},
'cbdinetime/production'	: {
					'title'		: 'CrackerBarrel Wait status - Production',
					'pageurl'	: 'https://hosted.where2getit.com/cgi-bin/dinetimewait.cgi',
				},
'cbdinetimereport/staging'	: {
					'title'		: 'CrackerBarrel Dinetime Report - Staging',
					'pageurl'	: 'https://hosted.where2stageit.com/cgi-bin/dinetimereport.cgi',
				},
'cbdinetimereport/production'	: {
					'title'		: 'CrackerBarrel Dinetime Report - Production',
					'pageurl'	: 'https://hosted.where2getit.com/cgi-bin/dinetimereport.cgi',
				},
'socialdatadeduplication'	: {
					'title'		: 'Social Data Deduplication Report',
					'pageurl'	: 'https://preprod.where2getit.com/cgi-bin/socialgraphdedupreport.cgi',
				},
'socialdatasda'	: {
					'title'		: 'Social Data From SDA Report',
					'pageurl'	: 'https://preprod.where2getit.com/cgi-bin/socialgraphsdareport.cgi',
				},
'brokenurls'	: {
					'title'		: 'Detect Broken URLs',
					'pageurl'	: 'https://preprod.where2getit.com/cgi-bin/urlvalidator.cgi',
				},
'validatejson'	: {
					'title'		: 'JSON Validator',
					'pageurl'	: 'https://hosted.where2getit.com/jsonTest-simple.html',
				},
};
