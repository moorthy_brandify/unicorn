MAPPINGS = {
	'devl'	: {
		'apikey'		: 'bde28db1-fe17-4e39-8da0-84f5c245affe',
		'dbqueryurl'		: 'http://zeus.dev.where2getit.com/cgi-bin/dbquery.cgi',
		'clearcacheurl'		: 'http://preprod.where2getit.com/cgi-bin/clearcache.cgi',
	},
	'stag'	: {
		'apikey'		: 'bde28db1-fe17-4e39-8da0-84f5c245affe',
		'dbqueryurl'		: 'http://staging.mtp.mgmt.where2getit.com/cgi-bin/dbquery.cgi',
		'clearcacheurl'		: 'http://preprod.where2getit.com/cgi-bin/clearcache.cgi',
	},
	'prod'	: {
		'apikey'		: 'bde28db1-fe17-4e39-8da0-84f5c245affe',
		'dbqueryurl'		: 'http://preprod.where2getit.com/cgi-bin/dbquery.cgi',
		'clearcacheurl'		: 'http://preprod.where2getit.com/cgi-bin/clearcache.cgi',
		'commandurl'		: 'http://hosted.where2getit.com/cgi-bin/runcmd.cgi',
	},
};

PULLREPORT = {
	'uploadqueue' : {
		'devl' : {
			'title' : 'File Upload Queue - Development',
			'url'   :'http://zeus.dev.where2getit.com/templates/upload-queue-data.tmpl?system=dev',
		},
		'stag' : {
			'title' : 'File Upload Queue - Staging',
			'url'   :'http://zeus.dev.where2getit.com/templates/upload-queue-data.tmpl?system=stag',
		},
		'prod' : {
			'title' : 'File Upload Queue - Production',
			'url'   : 'http://zeus.dev.where2getit.com/templates/upload-queue-data.tmpl?system=prod',
		},
	},
}

EXTERNALPAGE = {
	'socialdatadeduplication': {
		'title' : 'Social Data Deduplication Report',
		'url'   : 'https://preprod.where2getit.com/cgi-bin/socialgraphdedupreport.cgi',
	}, 
	'googlesearchbyphone': {
		'title' : 'Google Search By Phone Report',
		'url'   : 'https://preprod.where2getit.com/cgi-bin/googlesearchbyphone.cgi',
	}, 
	'gmbapiquerypage': {
		'title' : '',
		'url'   : 'https://zeus.dev.where2getit.com/api/GoogleMyBusinessAPI.html',
	}, 
	'socialdatasda': {
		'title' : 'Social Data From SDA',
		'url'   : 'https://preprod.where2getit.com/cgi-bin/socialgraphsdareport.cgi',
	}, 
	'cbdinetimestag': {
		'title' : 'CrackerBarrel Wait status - Staging',
		'url'   : 'https://hosted.where2stageit.com/cgi-bin/dinetimewait.cgi',
	},
	'cbdinetimeprod' : {
		'title' : 'CrackerBarrel Wait status - Production',
		'url'   : 'https://hosted.where2getit.com/cgi-bin/dinetimewait.cgi',
	},
	'querymappingsdevl': {
		'title'	: 'Upload and LDS Mappings - Development',
		'url'	: 'https://zeus.dev.where2getit.com/templates/query-locator.tmpl?system=dev',
	},
	'querymappingsstag': {
		'title'	: 'Upload and LDS Mappings - Staging',
		'url'	: 'https://zeus.dev.where2getit.com/templates/query-locator.tmpl?system=stag',
	},
	'querymappingsprod': {
		'title'	: 'Upload and LDS Mappings - Production',
		'url'	: 'https://zeus.dev.where2getit.com/templates/query-locator.tmpl?system=prod',
	},
	'queryauditdevl': {
		'title'	: 'Query Audit - Development',
		'url'	: 'https://zeus.dev.where2getit.com/templates/query-audit.tmpl?sys=dev',
	},
	'queryauditstag': {
		'title'	: 'Query Audit - Staging',
		'url'	: 'https://zeus.dev.where2getit.com/templates/query-audit.tmpl?sys=stag',
	},
	'queryauditprod': {
		'title'	: 'Query Audit - Production',
		'url'	: 'https://zeus.dev.where2getit.com/templates/query-audit.tmpl?sys=prod',
	},
	'urlvalidatorprod': {
		'title'	: 'URL Validator Application',
		'url'	: 'https://preprod.where2getit.com/cgi-bin/urlvalidator.cgi',
	},
}
