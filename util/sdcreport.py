from django.http import HttpResponse, JsonResponse
from django.template import loader, RequestContext
from django.shortcuts import render, render_to_response, redirect
from django.views.decorators.csrf import csrf_exempt

from main.session import Context
import constants.environments

import json
import urllib2
import base64
from urllib2 import HTTPError
import re

templateData = None
appAction = None
appParams = None

@csrf_exempt
def handle(request, action=None, params=None):

	global templateData, appAction, appParams

	templateData = Context(request, 'templatevars')
	appAction = action
	appParams = params

	if (appAction == 'show' or appAction == None):
		template = loader.get_template('util/sdcreport.html')
		templateData.setData('title','Social Graph Report')
		return HttpResponse(template.render(templateData.summarizeContext(), request))
	elif (appAction == 'json' and appParams == 'search'):
		jsonRequest = None
		try:
			jsonRequest = json.loads(request.body)
		except:
			return JsonResponse({'status' : 'NOT OK', 'message' : 'JSON Parse Error'})

		schemaName  = jsonRequest['schemaname']
		graphSource = jsonRequest['graphsource']
		isClaimed   = jsonRequest['isclaimed']

		if (schemaName == None or graphSource == None):
			return JsonResponse({'status' : 'NOT OK', 'message' : 'Inputs parameters missing'})

		if (not re.match(r'^\w+$', schemaName)):
			return JsonResponse({'status' : 'NOT OK', 'message' : 'Schema validation failed'})
		
		claimedCondition = ''
		if  (isClaimed == 'Y'):
			claimedCondition = ' AND isclaimed is TRUE '
		elif (isClaimed == 'N'):
			claimedCondition = ' AND isclaimed is FALSE '

		environment = "prod"
		outputJson  = {}
		
		apiKey = constants.environments.MAPPINGS[environment]['apikey']
		apiUrl = constants.environments.MAPPINGS[environment]['dbqueryurl']

		try:
			searchSql = """select '#SCHEMA#' as schema, graphsource, uid, clientkey, graphid, isclaimed, isprimary, effectivedate,
extragraphid1, extragraphid2, name, address1, city, state, postalcode, country, url
FROM #SCHEMA#.sociallocator
where graphsource='#GRAPHSOURCE#' #ISCLAIMED#"""

			searchSql = searchSql.replace('#SCHEMA#', schemaName)
			searchSql = searchSql.replace('#ISCLAIMED#', claimedCondition)
			searchSql = searchSql.replace('#GRAPHSOURCE#', graphSource)
			print searchSql;

			remoteJsonReq = {
				'apikey' : apiKey,
				'query'  : searchSql
			}

			reqObj = urllib2.Request(apiUrl)
			reqObj.add_header('Content-Type', 'application/json')
			resObj = urllib2.urlopen(reqObj, json.dumps(remoteJsonReq))
			resCode = resObj.getcode()
			if (resCode == 200):
				respMsg  = resObj.read()
				respJson = json.loads(respMsg)
				if (respJson['status'] == 200):
					outputJson['status'] = 'OK'
					outputJson['data']   = respJson['data']
					outputJson['count']  = respJson['count']
				else:
					raise Exception('Backend API Error')
			else:
				raise Exception('Backend API Error')

		except Exception as e:
			return JsonResponse({'status' : 'NOT OK', 'message' : 'Fetch Error'})

		return JsonResponse(outputJson)


def applist(request):

	global templateData

	if  (templateData == None):
		templateData = Context(request, 'templatevars')

	template = loader.get_template('util/applist.html')
	templateData.setData('title','W2GI - Utilities')

	return HttpResponse(template.render(templateData.summarizeContext(), request))

