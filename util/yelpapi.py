from django.http import HttpResponse, JsonResponse
from django.template import loader, RequestContext
from django.shortcuts import render, render_to_response, redirect
from django.views.decorators.csrf import csrf_exempt

from main.session import Context
import constants.environments

import json
import urllib2
import base64
from urllib2 import HTTPError

templateData = None
appAction = None
appParams = None

yelpConfig = {
   'userName' : 'brandify_data_ingestion',
   'passWord' : 'w40Lq5lcnjYtucnb',
   'url'      : {
   	'ingestapi'   : 'https://partner-api.yelp.com/v1/ingest',
   	'bizclaimapi' : 'https://partner-api.yelp.com/v1/partner_biz_claim',
	'bizinfoapi'  : 'https://api.yelp.com/v3',
    },
}

@csrf_exempt
def handle(request, action=None, params=None):

	global templateData, appAction, appParams

	templateData = Context(request, 'templatevars')
	appAction = action
	appParams = params

	yelpApi   = None
	yelpJobId = None
        if (request.method != 'POST'):
		showPage = True
		if (appParams != None):
			try:
				paramList = appParams.split('/');
				yelpApi   = paramList[0];
				yelpJobId = paramList[1];
			except:
				pass
			if (yelpApi != None and yelpJobId != None):
				showPage = False

		if showPage == True:
			return showYelpApiUtil(request, appParams)

	if (yelpApi == None or yelpJobId == None):
		yelpApi     = request.POST["yelpapi"]
		yelpJobId   = request.POST["yelpjobid"]

        if (len(yelpJobId) == 0):
		return showYelpApiUtil(request, appParams)

        templateData.setData('yelpjobid', yelpJobId)
        templateData.setData('yelpapi', yelpApi)
	templateData.setData('title', 'Yelp API Utility')

	responseStatus = 0
	if (yelpApi == 'jobstatus'):
		jobStatus   = getJobStatus(yelpJobId)
		claimStatus = getBizClaimtatus(yelpJobId)

		if (jobStatus != None):
			responseStatus = 1
			templateData.setData('jobstatus_response', json.dumps(json.loads(jobStatus), indent=4, sort_keys=True))
			if (claimStatus != None):
				templateData.setData('claimstatus_response', json.dumps(json.loads(claimStatus), indent=4, sort_keys=True))
			else:
				templateData.setData('claimstatus_response', 'Error fetching claim status may be due to invalid Job Id.')
	elif (yelpApi == 'businessinfo'):
		bizInfo = getBizInfo(yelpJobId)
		#jobStatus   = getJobStatus(yelpJobId)
		if (bizInfo != None):
			responseStatus = 1
			templateData.setData('bizinfo_response', json.dumps(json.loads(bizInfo), indent=4, sort_keys=True))
	
	templateData.setData('response_status', responseStatus)

	template = loader.get_template('util/yelpapi.html')
	return HttpResponse(template.render(templateData.summarizeContext(), request))


def getBizInfo(encid):

	endPoint = "businesses"
	apiUrl   =  yelpConfig["url"]["bizinfoapi"] + "/" + endPoint + "/" + encid
	return getYelpApi(apiUrl)
	
def getBizClaimtatus(jobid):

	endPoint = "request/status"
	apiUrl   =  yelpConfig["url"]["bizclaimapi"] + "/" + endPoint + "/" + jobid
	return getYelpPartnerApi(apiUrl)

def getJobStatus(jobid):

	endPoint = "status"
	apiUrl   =  yelpConfig["url"]["ingestapi"] + "/" + endPoint + "/" + jobid
	return getYelpPartnerApi(apiUrl)

def getYelpApi(requestUrl):

	apiKey = constants.environments.MAPPINGS['prod']['apikey']
	apiUrl = constants.environments.MAPPINGS['prod']['dbqueryurl']
	runSql = "select * from w2gi.api_capacity where apiname='sda.yelp'"

	apiConfig = None
	try:
		remoteJsonReq = {
			'apikey' : apiKey,
			'query'  : runSql
		}

		reqObj = urllib2.Request(apiUrl)
		reqObj.add_header('Content-Type', 'application/json')
		resObj = urllib2.urlopen(reqObj, json.dumps(remoteJsonReq))
		resCode = resObj.getcode()
		if (resCode == 200):
			respMsg  = resObj.read()
			respJson = json.loads(respMsg)
			if (respJson['status'] == 200):
				apiConfig = json.loads(respJson['data'][0]['apiconfig'])
			else:
				raise Exception('Backend API Error')
		else:
			raise Exception('Backend API Error')
		resObj.close()
	except Exception as e:
		print e

	if apiConfig == None:
		return;

	accessToken = apiConfig['access_token']
	tokenType   = apiConfig['token_type']

	request = urllib2.Request(requestUrl)
	request.add_header("Authorization", "%s %s" % (tokenType, accessToken))
	responseStr = None
	response    = None
	try:
		response = urllib2.urlopen(request)
		print "the response code is %s"%(response.getcode())
		if (response.getcode() == 200):
			responseStr = response.read()
		response.close()
	except HTTPError as he:
		return '{ "errorcode" : "%s", "errortext" : "%s" }'%(he.code, he.reason)
	except Exception as e:
		return None

	return responseStr

def getYelpPartnerApi(requestUrl):

	request = urllib2.Request(requestUrl)
	base64string = base64.b64encode('%s:%s' % (yelpConfig["userName"], yelpConfig["passWord"]))
	request.add_header("Authorization", "Basic %s" % base64string)
	responseStr = None
	try:
		response = urllib2.urlopen(request)
		if (response.getcode() == 200):
			responseStr = response.read()
	except:
		return None

	return responseStr

def showYelpApiUtil(request, params):

	global templateData, appAction, appParams

	if (appAction == 'show' or appAction == None):
		template = loader.get_template('util/yelpapi.html')
		templateData.setData('title','Yelp API Utility')
		return HttpResponse(template.render(templateData.summarizeContext(), request))

def applist(request):

	global templateData

	if  (templateData == None):
		templateData = Context(request, 'templatevars')

	template = loader.get_template('util/applist.html')
	templateData.setData('title','W2GI - Utilities')

	return HttpResponse(template.render(templateData.summarizeContext(), request))

