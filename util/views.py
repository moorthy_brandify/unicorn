from django.http import HttpResponse, JsonResponse
from django.template import loader, RequestContext
from django.shortcuts import render, render_to_response, redirect
from django.views.decorators.csrf import csrf_exempt

from main.session import Context
import constants.environments

import re
import json
import urllib2

templateData = None
appAction = None
appParams = None

@csrf_exempt
def runapp(request, appname, action=None, params=None):

	global templateData, appAction, appParams

	templateData = Context(request, 'templatevars')
	appAction = action
	appParams = params

	if appname == 'appkeyinfo':
		return appkeyinfo(request)
	if appname == 'sdcreport':
		import sdcreport
		return sdcreport.handle(request, action, params);
	if appname == 'dinetimestats':
		return dinetimestats(request)
	if appname == 'clearcache':
		return clearcache(request)
	if appname == 'refreshtemplate':
		return refreshtemplate(request)
	if appname == 'applist':
		return applist(request)
	if appname == 'pull':
		return pullreport(request)
	if appname == 'epage':
		return externalpage(request)
	if appname == 'yelpapi':
		import yelpapi;
		return yelpapi.handle(request, action, params);
	if appname == 'yelpreport':
		import yelpreport
		return yelpreport.handle(request, action, params);
	if appname == 'categoryengine':
		import categoryengine
		return categoryengine.handle(request, action, params)
	if appname == 'financereport':
		import financereport
		return financereport.handle(request, action, params)

	return applist(request)

def externalpage(request):

	global templateData, appAction, appParams
	template = loader.get_template('util/iframepage.html')
	if (appAction == None or not constants.environments.EXTERNALPAGE.has_key(appAction)):
		templateData.setData('title','May be you landed by mistake here!!')
		return HttpResponse(template.render(templateData.summarizeContext(), request))
	extAppCfg = constants.environments.EXTERNALPAGE[appAction];
	templateData.setData('title', extAppCfg['title'])
	templateData.setData('url', extAppCfg['url'])
	templateData.setData('name', appAction)
	return HttpResponse(template.render(templateData.summarizeContext(), request))

def pullreport(request):
	global templateData, appAction, appParams
	template = loader.get_template('util/pullreport.html')
	if (appAction == None or not constants.environments.PULLREPORT.has_key(appAction) or appParams == None):
		templateData.setData('title','May be you landed by mistake here!!')
		return HttpResponse(template.render(templateData.summarizeContext(), request))

	params = appParams.split('/')
	environments = reportAction = None
	environment  = params[0];
	if len(params) > 1:
		reportAction = params[1];

	if (not constants.environments.PULLREPORT[appAction].has_key(environment)):
		templateData.setData('title','Invalid Environment [%s]'%(environment))
		return HttpResponse(template.render(templateData.summarizeContext(), request))
	
	pullAppCfg = constants.environments.PULLREPORT[appAction][environment]

	if reportAction == None or not len(reportAction):
		templateData.setData('title', pullAppCfg['title'])
		return HttpResponse(template.render(templateData.summarizeContext(), request))

	reportContent = None
	try:
		reportContent = urllib2.urlopen(pullAppCfg['url']).read()
	except:
		reportContent = 'Error fetching report'
		

	return HttpResponse(reportContent, request)

def dinetimestats(request):
	global templateData, appAction, appParams
	if (appAction == 'show' or appAction == None):
		template = loader.get_template('util/dinetimestats.html')
		templateData.setData('title','CrackerBarrel Dinetime Statistics')
		return HttpResponse(template.render(templateData.summarizeContext(), request))

	elif (appAction == 'json'):
		jsonRequest = None
		try:
			jsonRequest = json.loads(request.body)
		except:
			return JsonResponse({'status' : 'NOT OK', 'message' : 'JSON Parse Error'})

		clientkey    = jsonRequest['clientkey']
		environment  = jsonRequest['environment']
		bylocation   = jsonRequest['bylocation']
		sdate        = jsonRequest['sdate']
		edate        = jsonRequest['edate']
		outputJson   = {}

		apiKey = constants.environments.MAPPINGS[environment]['apikey']
		apiUrl = constants.environments.MAPPINGS[environment]['dbqueryurl']

		wherecondition = " and requestdate >= '%s' and requestdate <= '%s' "%(sdate, edate)
		if (clientkey.lower().find('all') != 0):
			wherecondition += "and clientkey = '%s' " %(clientkey)

		sql = ''
		if (bylocation == '1'):
			sql = """with dl as (select requestdate, clientkey,
                       count (case when dinetimeevent='addWaitList' then dinetimeevent end) as addbooking,
                       count (case when dinetimeevent='modifyWaitList' then dinetimeevent end) as modifybooking,
                       count (case when dinetimeevent='cancelWaitList' then dinetimeevent end) as cancelbooking,
                       count(uid) as totalapi
                from crackerbarrel.dinetimelog
                where responsecode='200' %s
                group by requestdate, clientkey
                ),
                dvl as ( 
                select requestdate, clientkey,
                       sum(partysize) as bookingcount
                from crackerbarrel.dinetimevisitlog
                where 1=1 %s
                group by requestdate, clientkey
                )
                select dl.requestdate, dl.clientkey, dl.addbooking, dl.modifybooking, dl.cancelbooking, dl.totalapi,
                dvl.bookingcount from dl, dvl where dl.requestdate=dvl.requestdate and dl.clientkey=dvl.clientkey order by 1 asc, 6 desc
""" %(wherecondition, wherecondition)
		else:
                        sql = """
                with dl as (select requestdate, 
                       count (case when dinetimeevent='addWaitList' then dinetimeevent end) as addbooking,
                       count (case when dinetimeevent='modifyWaitList' then dinetimeevent end) as modifybooking,
                       count (case when dinetimeevent='cancelWaitList' then dinetimeevent end) as cancelbooking,
                       count(uid) as totalapi
                from crackerbarrel.dinetimelog
                where responsecode='200' %s
                group by requestdate
                ),
                dvl as ( 
                select requestdate,
                       sum(partysize) as bookingcount
                from crackerbarrel.dinetimevisitlog
                where 1=1 %s
                group by requestdate
                )
                select dl.requestdate, dl.addbooking, dl.modifybooking, dl.cancelbooking, dl.totalapi,
                dvl.bookingcount from dl, dvl where dl.requestdate=dvl.requestdate order by 1
""" %(wherecondition, wherecondition)

		try:
			remoteJsonReq = {
				'apikey' : apiKey,
				'query'  : sql 
			}

			reqObj = urllib2.Request(apiUrl)
			reqObj.add_header('Content-Type', 'application/json')
			resObj = urllib2.urlopen(reqObj, json.dumps(remoteJsonReq))
			resCode = resObj.getcode()
			if (resCode == 200):
				respMsg  = resObj.read()
				respJson = json.loads(respMsg)
				if (respJson['status'] == 200):
					outputJson['status'] = 'OK'
					outputJson['data']   = respJson['data']
					outputJson['count']  = respJson['count']
				else:
					raise Exception('Backend API Error')
			else:
				raise Exception('Backend API Error')

		except Exception as e:
			print e
			return JsonResponse({'status' : 'NOT OK', 'message' : 'Fetch Error'})

		return JsonResponse(outputJson)


def clearcache(request):
	global templateData, appAction, appParams

	if (appAction == 'show' or appAction == None):
		template = loader.get_template('util/clearcache.html')
		templateData.setData('title','Clear Varnish Cache')
		return HttpResponse(template.render(templateData.summarizeContext(), request))
	if (appAction == 'json'):
		jsonRequest = None
		try:
			jsonRequest = json.loads(request.body)
		except:
			return JsonResponse({'status' : 'NOT OK', 'message' : 'JSON Parse Error'})

		url     = jsonRequest['url']
		account = None
		if (jsonRequest.has_key('account')):
			account = jsonRequest['account']
		environment = 'prod'
		outputJson  = {}
		
		apiKey = constants.environments.MAPPINGS[environment]['apikey']
		apiUrl = constants.environments.MAPPINGS[environment]['clearcacheurl']

		try:
			remoteJsonReq = {
				'apikey' : apiKey,
				'url'    : url
			}
			if (account != None):
				remoteJsonReq['account'] = account

			reqObj = urllib2.Request(apiUrl)
			reqObj.add_header('Content-Type', 'application/json')
			resObj = urllib2.urlopen(reqObj, json.dumps(remoteJsonReq))
			resCode = resObj.getcode()
			if (resCode == 200):
				respMsg  = resObj.read()
				respJson = json.loads(respMsg)
				if (respJson['status'] == 200):
					outputJson['status'] = 'OK'
					outputJson['message'] = respJson['message']
				else:
					raise Exception('Backend API Error')
			else:
				raise Exception('Backend API Error')

		except Exception as e:
			print e
			return JsonResponse({'status' : 'NOT OK', 'message' : 'Clearcache Error'})

		return JsonResponse(outputJson)

def refreshtemplate(request):
	global templateData, appAction, appParams

	if (appAction == 'show' or appAction == None):
		template = loader.get_template('util/refreshtemplate.html')
		templateData.setData('title','Refresh Dynamic Templates')
		return HttpResponse(template.render(templateData.summarizeContext(), request))
	if (appAction == 'json'):
		jsonRequest = None
		try:
			jsonRequest = json.loads(request.body)
		except:
			return JsonResponse({'status' : 'NOT OK', 'message' : 'JSON Parse Error'})

		account = None
		if (jsonRequest.has_key('account')):
			account = jsonRequest['account']
		if account == None:
			return JsonResponse({'status' : 'NOT OK', 'message' : 'Account Name not provided'})
		if not re.match(r"^\w+$",account):
			return JsonResponse({'status' : 'NOT OK', 'message' : 'Invalid account name'})

		environment = 'prod'
		outputJson  = {}
		
		apiKey = constants.environments.MAPPINGS[environment]['apikey']
		apiUrl = constants.environments.MAPPINGS[environment]['commandurl']

		try:
			
			remoteJsonReq = {
				'apikey' : apiKey,
				'command'    : '/u/client/%s/refresh.sh'%(account)
			}

			reqObj = urllib2.Request(apiUrl)
			reqObj.add_header('Content-Type', 'application/json')
                        reqObj.add_header('IDENTIFIER', 'w2gi_internal_purpose')
			resObj = urllib2.urlopen(reqObj, json.dumps(remoteJsonReq))
			resCode = resObj.getcode()
			if (resCode == 200):
				respMsg  = resObj.read()
				respJson = json.loads(respMsg)
				if (respJson['status'] == 200):
					outputJson['status'] = 'OK'
					outputJson['message'] = respJson['message']
					outputJson['output'] = respJson['output']
				else:
					raise Exception('Backend API Error')
			else:
				raise Exception('Backend API Error')

		except Exception as e:
			print e
			return JsonResponse({'status' : 'NOT OK', 'message' : 'Dynamic Template Refresh Error'})

		return JsonResponse(outputJson)

def appkeyinfo(request):

	global templateData, appAction, appParams

	if (appAction == 'show' or appAction == None):
		template = loader.get_template('util/appkeyinfo.html')
		templateData.setData('title','W2GI AppKey Info')
		return HttpResponse(template.render(templateData.summarizeContext(), request))
	elif (appAction == 'json'):
		jsonRequest = None
		try:
			jsonRequest = json.loads(request.body)
		except:
			return JsonResponse({'status' : 'NOT OK', 'message' : 'JSON Parse Error'})

		inputStr    = jsonRequest['input']
		environment = jsonRequest['environment']
		outputJson  = {}
		
		apiKey = constants.environments.MAPPINGS[environment]['apikey']
		apiUrl = constants.environments.MAPPINGS[environment]['dbqueryurl']

		inputStr.replace('\'','')
		try:
			searchSql = None
			if (re.match (r'^\w+$', inputStr)): # schemaname
				searchSql = "select * from w2gi.locator where schemaname = '%s'"%(inputStr)
			elif (re.match(r'^\d+\.\d+.\d+.\d+.*$', inputStr)): # authip
				searchSql = "select * from w2gi.locator where authip ilike  '%s%%'"%(inputStr)
			elif (re.match(r'^http[s]*://.*$', inputStr, re.IGNORECASE)): #authurl
				searchSql = "select * from w2gi.locator where authurl ilike '%s%%'"%(inputStr)
			else:
				searchSql = "select * from w2gi.locator where appkey ilike '%%%s%%'"%(inputStr)

			remoteJsonReq = {
				'apikey' : apiKey,
				'query'  : searchSql
			}

			reqObj = urllib2.Request(apiUrl)
			reqObj.add_header('Content-Type', 'application/json')
			resObj = urllib2.urlopen(reqObj, json.dumps(remoteJsonReq))
			resCode = resObj.getcode()
			if (resCode == 200):
				respMsg  = resObj.read()
				respJson = json.loads(respMsg)
				if (respJson['status'] == 200):
					outputJson['status'] = 'OK'
					outputJson['data']   = respJson['data']
					outputJson['count']  = respJson['count']
				else:
					raise Exception('Backend API Error')
			else:
				raise Exception('Backend API Error')

		except Exception as e:
			return JsonResponse({'status' : 'NOT OK', 'message' : 'Fetch Error'})

		return JsonResponse(outputJson)
		
def blankapp(request):

	global templateData

	template = loader.get_template('util/blankapp.html')
	templateData.setData('title','W2GI Blank App')

	return HttpResponse(template.render(templateData.summarizeContext(), request))

def applist(request):

	global templateData

	if  (templateData == None):
		templateData = Context(request, 'templatevars')

	template = loader.get_template('util/applist.html')
	templateData.setData('title','W2GI - Utilities')

	return HttpResponse(template.render(templateData.summarizeContext(), request))

