from django.http import HttpResponse, JsonResponse
from django.template import loader, RequestContext
from django.shortcuts import render, render_to_response, redirect
from django.views.decorators.csrf import csrf_exempt

from main.session import Context
import constants.environments

import json
import urllib2
import base64
from urllib2 import HTTPError
import re

templateData = None
appAction = None
appParams = None

@csrf_exempt
def handle(request, action=None, params=None):

	global templateData, appAction, appParams

	templateData = Context(request, 'templatevars')
	appAction = action
	appParams = params

	if (appAction == 'byfeature' and appParams == None):
		template = loader.get_template('util/byfeature.html')

		environment = "prod"
		outputJson  = {}
		
		apiKey = constants.environments.MAPPINGS[environment]['apikey']
		apiUrl = constants.environments.MAPPINGS[environment]['dbqueryurl']

		productList = {}
		try:
			searchSql = "select uid, name  from w2gi.w2giproduct order by name asc"
			print searchSql

			remoteJsonReq = {
				'apikey' : apiKey,
				'query'  : searchSql
			}

			reqObj = urllib2.Request(apiUrl)
			reqObj.add_header('Content-Type', 'application/json')
			resObj = urllib2.urlopen(reqObj, json.dumps(remoteJsonReq))
			resCode = resObj.getcode()
			if (resCode == 200):
				respMsg  = resObj.read()
				respJson = json.loads(respMsg)
				if (respJson['status'] == 200):
					for rec in respJson['data']:
						productList[rec['uid']] = rec['name']
				else:
					raise Exception('Backend API Error')
			else:
				raise Exception('Backend API Error')

		except Exception as e:
			pass

		templateData.setData('productlist', productList)
		templateData.setData('title','Search By Product Feature Report')
		return HttpResponse(template.render(templateData.summarizeContext(), request))
	elif (appAction == 'byfeature' and appParams == 'json'):
		jsonRequest = None
		try:
			jsonRequest = json.loads(request.body)
		except:
			return JsonResponse({'status' : 'NOT OK', 'message' : 'JSON Parse Error'})

		productUid = jsonRequest['productuid']

		if (productUid == None):
			return JsonResponse({'status' : 'NOT OK', 'message' : 'Inputs parameters missing'})

		environment = "prod"
		outputJson  = {}
		
		apiKey = constants.environments.MAPPINGS[environment]['apikey']
		apiUrl = constants.environments.MAPPINGS[environment]['dbqueryurl']

		try:
			searchSql = """Select
cl.uid as "ClientUID", cl.name as "CompanyName", cl.webaddress as "CompanyWebSite", cl.industry as "Industry",
ct.uid as "ContractUID", ct.contractnumber AS "ContractNumber", ct.name as "ContractName",  ct.description as "ContractDescription",
ct.startdate as "ContractStartDate", ct.enddate as "ContractEndDate", ct.annualfee as "ContractAnnualFee",
CASE
     WHEN ct.autorenew THEN 'AutoRenewal'::text
     ELSE 'No AutoRenewal'::text
 END AS "AutoRenewal",
 CASE
     WHEN ct.autorenew = false THEN 'N/A'::text
     WHEN ct.renewalfrequency IS NULL OR ct.renewalfrequencyuom IS NULL THEN 'N/A'::text
     WHEN ct.renewalfrequency <= 0 THEN 'N/A'::text
     WHEN ct.renewalfrequencyuom::text = 'year'::text THEN (12 * ct.renewalfrequency) || ' months'::text
     WHEN ct.renewalfrequencyuom::text = 'month'::text THEN ct.renewalfrequency ||
     CASE
         WHEN ct.renewalfrequency = 1 THEN ' month'::text
         ELSE ' months'::text
     END
     ELSE 'N/A'::text
 END AS "AutoRenewalFrequency",
ct.sowstatus as "SOWStatus",
w2gipr.name as "ProductName", w2gife.name as "FeatureName"
from w2gi.client cl, w2gi.contract ct, w2gi.clientw2giproduct ctpr, w2gi.w2giproduct w2gipr,
     w2gi.clientw2gifeature ctfe, w2gi.w2gifeature w2gife
where
   cl.uid=ct.clientuid and
   ct.uid=ctpr.contractuid and
   ctpr.w2giproductuid=w2gipr.uid and 
   ctpr.uid=ctfe.clientproductuid and
   ctfe.w2gifeatureuid=w2gife.uid and
   ct.sowstatus in ('Out of Contract', 'Fully Executed', 'Auto-Renewal Executed', 'Auto-renewal Executed') and
   w2gipr.uid='#PRODUCTUID#' and (w2gife.name not ilike '%DO NOT USE%' and w2gife.name not ilike '%NO LONGER%' 
   and w2gife.name not ilike '%BEFORE %')"""
			outputHeaders = ('ClientUID','CompanyName','CompanyWebSite','Industry','ContractUID','ContractNumber','ContractName','ContractDescription','ContractStartDate','ContractEndDate','ContractAnnualFee','AutoRenewal','AutoRenewalFrequency','SOWStatus','ProductName');

			searchSql = searchSql.replace('#PRODUCTUID#', productUid)
			print searchSql

			remoteJsonReq = {
				'apikey' : apiKey,
				'query'  : searchSql
			}

			reqObj = urllib2.Request(apiUrl)
			reqObj.add_header('Content-Type', 'application/json')
			resObj = urllib2.urlopen(reqObj, json.dumps(remoteJsonReq))
			resCode = resObj.getcode()
			if (resCode == 200):
				respMsg  = resObj.read()
				respJson = json.loads(respMsg)
				if (respJson['status'] == 200):
					outputJson['status'] = 'OK'
					dbRecords = respJson['data'];
					outputRecords = {}
					featureList  = {}
					for rec in dbRecords:
						featureName = rec['FeatureName']
						featureList[featureName] = 'Yes'
						del rec['FeatureName']
						if (not outputRecords.has_key(rec['ClientUID'])):
							outputRecords[rec['ClientUID']] = rec
						outputRecords[rec['ClientUID']][featureName] = 'Yes'

					outputData = []
					for recId in outputRecords.keys():
						for feature in featureList.keys():
							if (not outputRecords[recId].has_key(feature)):
								outputRecords[recId][feature] = 'No'
						outputData.append(outputRecords[recId])

					outputHeaders = outputHeaders + tuple(featureList.keys())
					outputJson['data']   = outputData
					outputJson['title']  = outputHeaders

				else:
					raise Exception('Backend API Error')
			else:
				raise Exception('Backend API Error')

		except Exception as e:
			print "exception is "  + str(e)
			return JsonResponse({'status' : 'NOT OK', 'message' : 'Fetch Error'})

		return JsonResponse(outputJson)


def applist(request):

	global templateData

	if  (templateData == None):
		templateData = Context(request, 'templatevars')

	template = loader.get_template('util/applist.html')
	templateData.setData('title','W2GI - Utilities')

	return HttpResponse(template.render(templateData.summarizeContext(), request))

