from django.http import HttpResponse, JsonResponse
from django.template import loader, RequestContext
from django.shortcuts import render, render_to_response, redirect
from django.views.decorators.csrf import csrf_exempt

from main.session import Context
import constants.environments

import json
import urllib2
import base64
from urllib2 import HTTPError
import re

templateData = None
appAction = None
appParams = None

@csrf_exempt
def handle(request, action=None, params=None):

	global templateData, appAction, appParams

	templateData = Context(request, 'templatevars')
	appAction = action
	appParams = params

	if (appAction == 'show' or appAction == None):
		template = loader.get_template('util/yelpreport.html')
		templateData.setData('title','Yelp Social Graphs')
		return HttpResponse(template.render(templateData.summarizeContext(), request))
	elif (appAction == 'json' and appParams == 'trigger'):
		jsonRequest = None
		try:
			jsonRequest = json.loads(request.body)
		except:
			return JsonResponse({'status' : 'NOT OK', 'message' : 'JSON Parse Error'})

		schemaName  = jsonRequest['schemaname']
		action      = jsonRequest['action']
		graphuids   = jsonRequest['graphuids']

		if (schemaName == None or action == None):
			return JsonResponse({'status' : 'NOT OK', 'message' : 'Inputs parameters missing'})

		if (not re.match(r'^\w+$', schemaName)):
			return JsonResponse({'status' : 'NOT OK', 'message' : 'Schema validation failed'})

		graphuids = re.sub(r'\s+','', graphuids)
		graphuids = re.sub(r',$', '', graphuids)
		if (graphuids and len(graphuids) > 0):
			pass
		else:
			return JsonResponse({'status' : 'NOT OK', 'message' : 'Graph Uids not provided'})
			
		command   = ''
		if (action == 'CLAIM'):
			command = '/u/w2gi/bin/yelpintegration/run_claiming_selective.pl'
		elif (action == 'REVOKE'):
			command = '/u/w2gi/bin/yelpintegration/run_revoke_selective.pl'
		elif (action == 'CLAIMBFILL'):
			command = '/u/w2gi/bin/yelpintegration/sdc-yelp-claim-backfill.pl'
		elif (action == 'CREATEBFILL'):
			command = '/u/w2gi/bin/yelpintegration/sdc-yelp-create-backfill.pl'
		else:
			return JsonResponse({'status' : 'NOT OK', 'message' : 'Invalid Action'})

		environment = "prod"
		outputJson  = {}
		
		apiKey = constants.environments.MAPPINGS[environment]['apikey']
		apiUrl = constants.environments.MAPPINGS[environment]['commandurl']

		try: 
			remoteJsonReq = {
				'apikey'  : apiKey,
				'command' : command,
				'params'  : "'%s' '%s'"%(schemaName, graphuids)
			}

			reqObj = urllib2.Request(apiUrl)
			reqObj.add_header('Content-Type', 'application/json')
			reqObj.add_header('IDENTIFIER', 'w2gi_internal_purpose')
			resObj = urllib2.urlopen(reqObj, json.dumps(remoteJsonReq))
			resCode = resObj.getcode()
			if (resCode == 200):
				respMsg  = resObj.read()
				respJson = json.loads(respMsg)
				if (respJson['status'] == 200):
					outputJson['status'] = 'OK'
					outputJson['output']   = respJson['output']
					outputJson['message']   = respJson['message']
				else:
					raise Exception('Backend API Error')
			else:
				raise Exception('Backend API Error')
		except Exception as e:
			return JsonResponse({'status' : 'NOT OK', 'message' : 'Fetch Error'})

		return JsonResponse(outputJson)
	elif (appAction == 'json' and appParams == 'search'):
		jsonRequest = None
		try:
			jsonRequest = json.loads(request.body)
		except:
			return JsonResponse({'status' : 'NOT OK', 'message' : 'JSON Parse Error'})

		schemaName  = jsonRequest['schemaname']
		reportType  = jsonRequest['reporttype']
		isClaimed   = jsonRequest['isclaimed']

		if (schemaName == None or reportType == None):
			return JsonResponse({'status' : 'NOT OK', 'message' : 'Inputs parameters missing'})

		if (not re.match(r'^\w+$', schemaName)):
			return JsonResponse({'status' : 'NOT OK', 'message' : 'Schema validation failed'})
		
		claimedCondition = ''
		if  (isClaimed == 'Y'):
			claimedCondition = ' AND isclaimed is TRUE '
		elif (isClaimed == 'N'):
			claimedCondition = ' AND isclaimed is FALSE '

		environment = "prod"
		outputJson  = {}
		
		apiKey = constants.environments.MAPPINGS[environment]['apikey']
		apiUrl = constants.environments.MAPPINGS[environment]['dbqueryurl']

		try:
			searchSql = """with graph as (
SELECT split_part(a.tableoid::regclass::text, '.', 1) as schema, a.uid, a.clientkey, a.isclaimed, split_part(a.status, ' - ', 1) as yelpaction, split_part(a.status, ' - ', 2) as yelpjobid, 
split_part(a.status, ' - ', 3) as yelpjobstatus,  a.effectivedate, a.graphid, a.extragraphid1, a.extragraphid2
FROM #SCHEMA#.socialgraph a
JOIN (SELECT clientkey, max(effectivedate) AS effectivedate, max(uid) AS uid
FROM #SCHEMA#.socialgraph
WHERE graphsource = 'yelpplaces' AND datasource in ('yelp','upload')
GROUP BY 1) b 
ON a.uid = b.uid 
WHERE a.status ilike '#REPORTTYPE#' #ISCLAIMED#)
select graph.*, store.name, store.address1, store.city, store.state, store.postalcode, store.country
from #SCHEMA#.storelocator_mv store,  graph where store.clientkey=graph.clientkey"""

			searchSql = searchSql.replace('#SCHEMA#', schemaName)
			searchSql = searchSql.replace('#ISCLAIMED#', claimedCondition)
			if (reportType == 'CREATE'):
				searchSql = searchSql.replace('#REPORTTYPE#', 'CREATE - %')
			elif (reportType == 'CLAIM'):
				searchSql = searchSql.replace('#REPORTTYPE#', 'CLAIM - %')
			else:
				searchSql = searchSql.replace('#REPORTTYPE#', '% - %')

			remoteJsonReq = {
				'apikey' : apiKey,
				'query'  : searchSql
			}

			reqObj = urllib2.Request(apiUrl)
			reqObj.add_header('Content-Type', 'application/json')
			resObj = urllib2.urlopen(reqObj, json.dumps(remoteJsonReq))
			resCode = resObj.getcode()
			if (resCode == 200):
				respMsg  = resObj.read()
				respJson = json.loads(respMsg)
				if (respJson['status'] == 200):
					outputJson['status'] = 'OK'
					outputJson['data']   = respJson['data']
					outputJson['count']  = respJson['count']
				else:
					raise Exception('Backend API Error')
			else:
				raise Exception('Backend API Error')

		except Exception as e:
			return JsonResponse({'status' : 'NOT OK', 'message' : 'Fetch Error'})

		return JsonResponse(outputJson)


def applist(request):

	global templateData

	if  (templateData == None):
		templateData = Context(request, 'templatevars')

	template = loader.get_template('util/applist.html')
	templateData.setData('title','W2GI - Utilities')

	return HttpResponse(template.render(templateData.summarizeContext(), request))

