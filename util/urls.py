from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^app/(?P<appname>\w+)/(?P<action>\w+)/(?P<params>.*)$', views.runapp),
    url(r'^app/(?P<appname>\w+)/(?P<action>\w+).*$', views.runapp),
    url(r'^app/(?P<appname>\w+).*$', views.runapp),
    url(r'^$', views.applist),
]
