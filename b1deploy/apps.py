from __future__ import unicode_literals

from django.apps import AppConfig


class B1DeployConfig(AppConfig):
    name = 'b1deploy'
