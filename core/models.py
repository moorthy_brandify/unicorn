from __future__ import unicode_literals

from django.db import models

# Create your models here.

class User(models.Model):

	db_table = 'user'

	import main.constants.countries
	SEXCODES = (
		('M', 'Male'),
		('F', 'Female'),
	)

	TITLES  = (
		('Mr', 'Mr.'),
		('Ms', 'Ms.'),
		('Mrs','Mrs.'),
	)

	COUNTRYLIST = main.constants.countries.COUNTRYLIST

	title = models.CharField(max_length=10, default='Mr')
	firstname = models.CharField(max_length=50, null=False)
	lastname = models.CharField(max_length=50, null=True)
	sex = models.CharField(max_length=1, choices=SEXCODES, null=True)
	dob = models.DateField(auto_now=False, null=True)
	emailid  = models.CharField(max_length=100, unique=True, null=True)
	mobileno = models.CharField(max_length=20, null=True)
	address1 = models.CharField(max_length=100, null=True)
	address2 = models.CharField(max_length=100, null=True)
	city = models.CharField(max_length=50, null=True)
	state = models.CharField(max_length=50, null=True)
	country  = models.CharField(max_length=5, default='US')
	zipcode = models.CharField(max_length=10, null=True)

	def __str__(self):
		return self.firstname

class Login(models.Model):

	db_table = 'login'

	user = models.ForeignKey(User, on_delete=models.CASCADE)
	username = models.CharField(max_length=100, primary_key=True);
	password  = models.CharField(max_length=128)
	lastlogin = models.DateTimeField(null=True)
	active    = models.BooleanField(default=True)

	def __str__(self):
		return self.username

class AuthRequests(models.Model):

	db_table = 'authrequests'

	userid = models.IntegerField()
	event  = models.CharField(max_length=24)
	uuid   = models.CharField(max_length=64, unique=True)
	data   = models.TextField()

	class Meta:
		unique_together = (('userid', 'event'),)

class Account(models.Model):

	db_table = 'account'

	ACCTYPES = (
		('INT', 'Internal'),
		('SYS', 'System'),
		('CLN', 'Client'),
		('HRD', 'HR Dept'),
		('FIN', 'Finance'),
		('SUP', 'Support'),
		('PER', 'Personal'),
	)

	code     = models.CharField(max_length=48, primary_key=True)
	name     = models.CharField(max_length=64, unique=True, db_index=True);
	fullname = models.CharField(max_length=100);
	acctype  = models.CharField(max_length=24, choices=ACCTYPES, default='INT')
	internal = models.BooleanField(default=False)

	def __str__(self):
		return self.name

class AlertCategory(models.Model):

	db_table = 'alertcategory'

	code = models.CharField(max_length=12, primary_key=True)
	name = models.CharField(max_length=24, unique=True)
	desc = models.CharField(max_length=64)

	def __str__(self):
		return self.code

class AlertType(models.Model):

	db_table = 'alerttype'

	code = models.CharField(max_length=12, primary_key=True)
	name = models.CharField(max_length=24, unique=True)
	desc = models.CharField(max_length=64)
	icon = models.CharField(max_length=48)

	def __str__(self):
		return self.code

class Alerts(models.Model):

	db_table = 'alerts'

	ALERTSTATUS = (
		('INFO', 'Info'),
		('SUCCESS', 'Success'),
		('WARN', 'Warning'),
		('ERROR', 'Error'),
	)

	uid       = models.AutoField(primary_key=True)
	category  = models.ForeignKey(AlertCategory)
	alerttype = models.ForeignKey(AlertType)
	account   = models.CharField(max_length=24, db_index=True)
	status    = models.CharField(max_length=16, db_index=True)
	message   = models.TextField()
	metadata  = models.TextField()
	eventts   = models.DateTimeField(null=True)

	def __str__(self):
		return self.uid

class B1DeployRequests(models.Model):

	db_table = 'b1deployrequests';

	uid       = models.AutoField(primary_key=True)
	environment = models.CharField(max_length=20, db_index=True)
