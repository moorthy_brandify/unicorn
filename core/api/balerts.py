from django.http import HttpResponse, JsonResponse
from django.template import loader, RequestContext
from django.shortcuts import render, render_to_response, redirect
from django.views.decorators.csrf import csrf_exempt

from main.session import Context
from core import models

import json
import re
import datetime

@csrf_exempt
def handle(request):

	try:
		jsonRequest = json.loads(request.body)
	except: 
		return JsonResponse({'status' : 'NOT OK', 'message' : 'JSON Parse Error'})

	action = jsonRequest["action"]
	apiResponse = None;
	if (action == 'newalert'):
		apiResponse = handleNewAlert(jsonRequest["data"])
	else:
		apiResponse = {
			'code' : '404',
			'mesg' : 'Unknown Action'
		}
		
	return JsonResponse(apiResponse)

def handleNewAlert(apiRequest):

	reqFields = ["account", "status", "message", "eventts", "alerttype", "alertcategory"]
	## Validate if all require field exists
	for key in reqFields:
		if not key in apiRequest:
			return { "code": '405', "mesg": "Attribute [" + key + "] is missing in the request" }

	eventTime = None
	try:
		eventTime = datetime.datetime.fromtimestamp(int(apiRequest["eventts"]));
	except:	
		return { "code": '405', "mesg": "Attribute 'eventts' doesnot contain a valid epoch timestamp in seconds" }
	
	
	try:
		alertTypeRecord = models.AlertType(code = apiRequest["alerttype"]);
		alertCategoryRecord = models.AlertCategory(code = apiRequest["alertcategory"]);
		alertRecord = models.Alerts(account = apiRequest["account"], status = apiRequest["status"], message = apiRequest["message"],
				metadata = json.dumps(apiRequest["metadata"]), eventts  = eventTime.strftime('%Y-%m-%d %H:%M:%S+00:00'), 
				alerttype = alertTypeRecord, category = alertCategoryRecord)
		alertRecord.save();

		apiResponse = {
			'code' : '200',
			'mesg' : 'Alert received',
			'info' : {
				'alert_id' : alertRecord.uid
			}
		}
	except:
		return { "code": '500', "mesg": "Unable to add this alert into the system" }

	return apiResponse
