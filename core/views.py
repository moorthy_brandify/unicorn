from django.http import HttpResponse
from django.template import loader, RequestContext
from django.shortcuts import render_to_response, redirect
from django.views.decorators.csrf import csrf_protect
from django.contrib.auth import authenticate as django_authenticate
from django.contrib.auth import login as django_login
from django.contrib.auth import logout as django_logout
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User as djangoUser

from main.session import Context
import main.utils.uid
import main.utils.mail

import datetime
import models
import json

templateData = None

def index(request):
	if (request.user != 'AnonymousUser'):
		return redirect('/home')
	redirect ('/login')

def login(request):
	template = loader.get_template('core/login.html')
	templateData = Context(request, 'templatevars')

	if ('login_action' in request.session):
		templateData.setData('login_action', request.session['login_action'])
	request.session['login_action'] = 'login'

	if 'next' in request.GET:
		templateData.setData('next', request.GET['next'])

	return HttpResponse(template.render(templateData.summarizeContext(), request))

@csrf_protect
def dologin(request):

	if (request.method != 'POST'):
		return redirect('/login/')

	csrfContext  = RequestContext(request)
	template     = loader.get_template('core/login.html')
	templateData = Context(request, 'templatevars')
	request.session['login_action'] = 'login'
	
	username = request.POST['username'].lower()
	password = request.POST['password']

	record = None
	try:
		record = models.Login.objects.get(username=username)
	except:
		record = None

	## Is authorised in our DB
	if (record == None):
		templateData.setData('login_failed', 1)
		return HttpResponse(template.render(templateData.summarizeContext(), request))

	## Authenticate using Djanho
	user = django_authenticate(username=username, password=password)

	if (user is None):
		templateData.setData('login_failed', 1)
		return HttpResponse(template.render(templateData.summarizeContext(), request))
		
	django_login(request, user)

	loginRecord = models.Login.objects.get(username=request.user)
	request.session['userinfo'] = {}
	request.session['userinfo']['username']  = loginRecord.username
	request.session['userinfo']['firstname'] = loginRecord.user.firstname
	request.session['userinfo']['lastname']  = loginRecord.user.lastname

	if 'next' in request.GET:
		return redirect(request.GET['next'])

	return redirect('/home/')

def dologout(request):

	django_logout(request)
	request.session['login_action'] = 'logout'
	return redirect('/login/')


@login_required(login_url='/login/')
def profile(request, action):
	template = loader.get_template('core/profile.html')

	if (action == 'save'):
		return saveprofile(request)

	record = models.Login.objects.get(username=request.user)
	templateData = Context(request, 'templatevars')
	templateData.setData('show_userdetails_form', 1)
	templateData.setData('profile_edit', 0)
	if (action == 'edit'):
		templateData.setData('profile_edit', 1)

	templateContext = templateData.summarizeContext()
	templateContext['user'] = record.user

	return HttpResponse(template.render(templateContext, request))
	
@csrf_protect
@login_required(login_url='/login/')
def saveprofile(request):

	if (request.method != 'POST'):
		return redirect('/core/profile/')
	template = loader.get_template('core/profile.html')

	record = models.Login.objects.get(username=request.user)
	templateData = Context(request, 'templatevars')

	record.user.title = request.POST['title']
	record.user.firstname = request.POST['firstname']
	record.user.lastname = request.POST['lastname']
	record.user.sex = request.POST['sex']
	record.user.address1 = request.POST['address1']
	record.user.address2 = request.POST['address2']
	record.user.city = request.POST['city']
	record.user.state = request.POST['state']
	record.user.country = request.POST['country']
	record.user.zipcode = request.POST['zipcode']
	record.user.emailid = request.POST['emailid']
	record.user.mobileno = request.POST['mobileno']
	record.user.dob = datetime.datetime.strptime(request.POST['dob'], '%m/%d/%Y').strftime('%Y-%m-%d')

	templateData.setData('show_userdetails_form', 1)
	try:
		record.user.save()
		record = models.Login.objects.get(username=request.user)
		templateData.setData('save_success', 1)
		templateData.setData('save_message', 'User Details saved successfully')
	except Exception as ex:
		templateData.setData('save_failed', 1)
		templateData.setData('save_message', 'Error occurred while saving User Details!!')

	templateData.setData('profile_edit', 0)

	templateContext = templateData.summarizeContext()
	templateContext['user'] = record.user
		
	return HttpResponse(template.render(templateContext, request))

@login_required(login_url='/login/')
def password (request, action):
	template = loader.get_template('core/profile.html')

	if (action == 'change'):
		return changepassword(request)

	return redirect('/core/profile/')

def register(request, action):
	actionparams = action.split('/')
	subaction    = actionparams[0]
	template = loader.get_template('core/registeruser.html')
	templateData = Context(request, 'templatevars')

	if (request.method == 'POST'):
		firstname = request.POST['firstname']
		lastname  = request.POST['lastname']
		mobileno  = request.POST['mobileno']
		emailid   = request.POST['emailid']
		username  = request.POST['username']
		newpassword        = request.POST['newpassword']
		newpasswordconfirm = request.POST['newpasswordconfirm']

		templateData.setData('firstname', firstname);
		templateData.setData('lastname', lastname);
		templateData.setData('mobileno', mobileno);
		templateData.setData('emailid', emailid);
		templateData.setData('username', username);

		errormsg = ''
		iserror   = False
		try:
			if (emailid.lower().rfind('@brandify.com') == -1  and emailid.lower().rfind('@where2getit.com') == -1):
				raise Exception('Error', 'Users with Brandify Email address alone can register online.')
			elif (not username.isalnum()):
				raise Exception('Error', 'Username should be alpha-numeric.')
			elif (newpassword != newpasswordconfirm):
				raise Exception('Error', 'Your password and Confirmation password doesnot match.')

			loginRecord = None
			try:
				loginRecord = models.Login.objects.get(username__iexact=username)
			except:
				pass
			
			if  loginRecord != None:
				raise Exception('Error', 'Username is already registered with Unicorn.')

			userRecord = None
			try:
				userRecord = models.User.objects.get(emailid__iexact=emailid)
			except:
				pass

			if userRecord  != None:
				raise Exception('Error', 'Email Address is already in use.')

		except Exception as err:
			errortype, errormsg = err.args
			iserror = True
				

		if iserror:
			templateData.setData('register_form', 1)
			templateData.setData('register_status', 'error')
			templateData.setData('register_message', errormsg)
			return HttpResponse(template.render(templateData.summarizeContext(), request))


		try:
			# Create DJango User
			newUser = djangoUser.objects.create_user(username, emailid, newpassword)
			newUser.first_name = firstname
			newUser.last_name  = lastname
			newUser.email      = emailid
			newUser.is_superuser = False
			newUser.is_staff     = True
			newUser.is_active    = True
			newUser.save()

			# Create App User
			newUser = models.User()
			newUser.firstname = firstname
			newUser.lastname  = lastname
			newUser.emailid   = emailid
			newUser.mobileno  = mobileno
			newUser.save()

			# Create App Login
			newLogin = models.Login()
			newLogin.username = username
			newLogin.active   = True
			newLogin.user     = newUser
			newLogin.save()
			
		except Exception as exc:
			print exc;
			templateData.setData('register_form', 1)
			templateData.setData('register_status', 'error')
			templateData.setData('register_message', 'Internal Error!! Please contact Brandify Tech Support!')
			return HttpResponse(template.render(templateData.summarizeContext(), request))

		templateData.setData('register_info', 1)
		templateData.setData('register_status', 'success')
		templateData.setData('register_message', 'User Registeration is successful!')

		return HttpResponse(template.render(templateData.summarizeContext(), request))

	else:
		templateData.setData('register_form', 1)

		return HttpResponse(template.render(templateData.summarizeContext(), request))


def resetpassword(request, action):
	actionparams = action.split('/')
	subaction    = actionparams[0]
	template = loader.get_template('core/resetpassword.html')
	templateData = Context(request, 'templatevars')
	if (subaction == 'prompt'):
		templateData.setData('reset_password_prompt', 1)
		if (request.method != 'POST'):
			return HttpResponse(template.render(templateData.summarizeContext(), request))
		else:
			rwpusername = request.POST['rwpusername']
			loginRecord = None
			userRecord  = None
			try:
				loginRecord = models.Login.objects.get(username__iexact=rwpusername)
			except:
				loginRecord = None

			try:
				userRecord = models.User.objects.get(emailid__iexact=rwpusername)
			except:
				userRecord = None

			if (loginRecord == None and userRecord == None):
				templateData.setData('reset_status', "error")
				templateData.setData('reset_message', "The entered Login name or Email address is invalid. Please try again  !!")
				return HttpResponse(template.render(templateData.summarizeContext(), request))

			emailAddr = None
			if userRecord:
				emailAddr = userRecord.emailid
			else:
				userRecord = models.User.objects.get(id=loginRecord.user_id)
				emailAddr = userRecord.emailid

			if loginRecord == None:
				loginRecord = models.Login.objects.get(user_id=userRecord.id)

			displayEmailAddr = emailAddr[:2] + len(emailAddr[2:emailAddr.index('@')]) * 'x' + emailAddr[emailAddr.index('@'):]
			uuidVal = main.utils.uid.generate_uuid().hex

			##  Delete any old resetpassword event for this user id
			authreqRecord = None
			try:
				authreqRecord = models.AuthRequests.objects.get(userid=userRecord.id, event='resetpassword')
				authreqRecord.delete()
			except:
				authreqRecord = None

			requestData =  {
				'user'  : loginRecord.username,
				'emailaddress'  : userRecord.emailid,
				'redirectcontext'  : '/core/resetpassword/form'
			}

			import json;
			authreqRecord = models.AuthRequests(userid = userRecord.id, event = 'resetpassword', uuid=uuidVal, data=json.dumps(requestData))
			authreqRecord.save()
			
			mailInfo = {
				'to' : userRecord.emailid,
				'contenttype' : 'html',
				'subject' : 'Reset your Unicorn password',
				'message' : """
Dear %s %s,<br/>
<p>
&nbsp;&nbsp;&nbsp;&nbsp;You have opted for resetting your Unicorn password. Please click on this <br/>
reset password <a href="http://unicorn.dev.where2getit.com/authrequest/%s"> link </a> to reset your password.
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;Please ignore if you have not opted to change your password.
<br/>
</p>
Thanks<br/>
- Brandify<br/>
""" %(userRecord.firstname, userRecord.lastname, uuidVal)
			}
			main.utils.mail.sendemail(mailInfo)
			templateData.setData('reset_status', "success")
			templateData.setData('reset_message', "The reset password link has been sent your Email address %s" %(displayEmailAddr))
			return HttpResponse(template.render(templateData.summarizeContext(), request))
	elif subaction == 'form':
		templateData.setData('reset_password_form', 1)
		if (request.method != 'POST'):
			templateData.setData('authrequestid', request.session['authrequestid'])
			return HttpResponse(template.render(templateData.summarizeContext(), request))
		else:
			authrequestid = request.POST['authrequestid']
			newpassword     = request.POST['newpassword']
			newpasswordconfirm = request.POST['newpasswordconfirm']
			authrequestRecord = None

			templateData.setData('authrequestid', authrequestid)

			try:
				authrequestRecord = models.AuthRequests.objects.get(uuid=authrequestid)
			except Exception as ex:
				templateData.setData('reset_status', "error")
				templateData.setData('reset_message', 'Unable to change your password. Please retry later!!')
				return HttpResponse(template.render(templateData.summarizeContext(), request))


			import json
			requestData = json.loads(authrequestRecord.data)
			resetpswdUser = requestData['user']

			try:
				userObject  = djangoUser.objects.get(username=requestData['user'])
				userObject.set_password(newpassword)
				userObject.save()
				userObject.check_password(newpassword)
			except Exception as ex:
				templateData.setData('reset_status', "error")
				templateData.setData('reset_message', 'Unable to change your password. Please retry later!!')
				print ex
				return HttpResponse(template.render(templateData.summarizeContext(), request))

			authrequestRecord.delete()

			templateData.setData('reset_password_form', 0)
			templateData.setData('reset_password_success', 1)
			templateData.setData('reset_status', "success")
			templateData.setData('reset_message', 'Your Password resetted successfully.')

			return HttpResponse(template.render(templateData.summarizeContext(), request))
	else:
		template = loader.get_template('core/resetpassword.html')
		return HttpResponse(template.render(templateData.summarizeContext(), request))
	

@csrf_protect
@login_required(login_url='/login/')
def changepassword (request):

	if (request.method != 'POST'):
		return redirect('/core/profile/')
	template = loader.get_template('core/profile.html')

	templateData = Context(request, 'templatevars')

	currentpassword = request.POST['currentpassword']
	newpassword     = request.POST['newpassword']
	newpasswordconfirm = request.POST['newpasswordconfirm']

	record = None
	user = None
	try:
		record = models.Login.objects.get(username=request.user)
		user = django_authenticate(username=request.user, password=currentpassword)
	except Exception as ex:
		record = None
		user = None

	templateData.setData('show_password_form', 1)
	templateData.setData('profile_edit', 0)
	## Is authorised in our DB
	if (record == None or user == None):
		templateData.setData('save_failed', 1)
		templateData.setData('save_message', 'Incorrect password provided. Password not changed!!')
		return HttpResponse(template.render(templateData.summarizeContext(), request))

	if (newpassword != newpasswordconfirm):
		templateData.setData('save_failed', 1)
		templateData.setData('save_message', 'New and confirmation passwords are not same!!')
		return HttpResponse(template.render(templateData.summarizeContext(), request))

	
	try:
		userObject  = djangoUser.objects.get(username=request.user)
		userObject.set_password(newpassword)
		userObject.save()
		userObject.check_password(newpassword)
	except Exception as ex:
		templateData.setData('save_failed', 1)
		templateData.setData('save_message', 'Unable to change your password. Please retry later!!')
		return HttpResponse(template.render(templateData.summarizeContext(), request))

	templateData.setData('save_success', 1)
	templateData.setData('save_message', 'Password changed successfully.')

	templateContext = templateData.summarizeContext()
	templateContext['user'] = record.user
		
	return HttpResponse(template.render(templateContext, request))
