from django.conf.urls import url

from . import views
from core.api import balerts as api_balerts


urlpatterns = [
    url(r'^login', views.login),
    url(r'^dologin', views.dologin),
    url(r'^dologout', views.dologout),
    url(r'^balerts/', api_balerts.handle),
    url(r'^profile/*(?P<action>.*)$', views.profile),
    url(r'^password/*(?P<action>.*)$', views.password),
    url(r'^resetpassword/*(?P<action>.*)$', views.resetpassword),
    url(r'^register/*(?P<action>.*)$', views.register),
    url(r'^$', views.index),
]
